﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
 using System.Data;
using System.Data.SqlClient;
using System.Configuration;


namespace webAssignmentR
{

    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                rdoMemberType.SelectedIndex = 0;

            }

        }

        protected void loginBtn_Click(object sender, EventArgs e)
        {
            string memberSelectedValue = rdoMemberType.SelectedValue;
            string pwd = tbPasswd.Text;
            string emailAddr = tbEmailAddr.Text;


            if (memberSelectedValue != "Admin")
            {
                DataSet res = DatabaseProxy.Shared.selectCmd(memberSelectedValue, $"SELECT * FROM {memberSelectedValue} WHERE "
                    + $"Password = '{pwd}' and EmailAddr = '{emailAddr}'");
                if (res.Tables[memberSelectedValue].Rows.Count > 0)
                {
                    // User exists
                    string path = "Private/";
                    string id = Convert.ToString(res.Tables[memberSelectedValue].Rows[0][memberSelectedValue + "ID"]);
                    string name = (string)res.Tables[memberSelectedValue].Rows[0][memberSelectedValue == "Parent" ? "ParentName" : "Name"];
                    switch (memberSelectedValue)
                    {
                        case "Mentor":
                            path += "Mentor/viewStudProf.aspx";
                            break;
                        case "Student":
                            path += "Student/editProf.aspx";
                            break;
                        case "Parent":
                            path += "Parent/viewChildProj.aspx";
                            break;
                        default: break;
                    }
                    // Set session state
                    Session["Username"] = name;
                    Session["UserID"] = id;
                    Session["UserType"] = memberSelectedValue;

                    Response.Redirect(path);
                }
                else
                {
                    lblErrorMsg.Text = "Login Failed";
                }


            }
            else if (memberSelectedValue == "Admin" && emailAddr == "admin@np.edu.sg" && pwd == "passAdmin")
            {
                Session["UserType"] = memberSelectedValue;
                Response.Redirect("Private/Admin/parentViewReq.aspx");

            }
            else
            {
                lblErrorMsg.Text = "Login Failed";
            }





        }

    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.Master" AutoEventWireup="true" CodeBehind="studentPortfolio.aspx.cs" Inherits="webAssignmentR.studentPortfolio" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="styles/portfolio.css" rel="stylesheet" />
    <div class="container-fluid">
        
            <form class="form-inline d-flex justify-content-center" style="flex-wrap:wrap"  runat="server">
                <div class="col-10" align="center" >
                    <asp:TextBox ID="txtSearch" runat="server" Width="85%" Height="30px" CssClass="searchBar" ></asp:TextBox>
                    </div>
                <div class="col-2">
                    <asp:Button CssClass="btn-primary" ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
                        
                    </div>
              <div class="col-12"><br /></div>
                <div class="col-12" align="center">
                        &nbsp;<asp:GridView ID="gvStudent" runat="server" AutoGenerateColumns="False" Width="90%" ForeColor="White" AllowPaging="True" BorderStyle="None" BorderWidth="0px" GridLines="None" Height="157px" OnPageIndexChanging="gvStudent_PageIndexChanging" PageSize="4" >
                            <Columns>
                                <asp:ImageField DataImageUrlField="Photo" DataImageUrlFormatString="./Images/Students/{0}" HeaderText="Profile Picture">
                                    <ControlStyle Height="100px" />
                                </asp:ImageField>
                                <asp:BoundField DataField="Name" HeaderText="Name" />
                                <asp:BoundField DataField="Course" HeaderText="Course" />
                            </Columns>
                        </asp:GridView>
                    </div>
                  
            </form>

    </div>
</asp:Content>

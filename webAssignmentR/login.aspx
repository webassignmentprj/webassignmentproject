﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.Master" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="webAssignmentR.login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="styles/login.css" rel="stylesheet" />

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-7 col-lg-8  content">
                <div >
                    <h1>
                        <span>
                            <img src="Images/image3.png" alt="infinityLogo">
                        </span>Infinite.ict_</h1>
                    <h2>NGEE ANN POLY</h2>
                    <p>WEB ASSIGNMENT</p>

                </div>

            </div>

            <div class=" col-sm-12 col-md-5 col-lg-4">
                <div class=" content">
                    <form id="form1" runat="server">
                        <!--Email address-->
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <asp:TextBox ID="tbEmailAddr" placeholder="Enter email" runat="server" CssClass="form-control form-control-lg"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqEmailAddr" runat="server" ControlToValidate="tbEmailAddr" ErrorMessage="Please enter a emai address" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="regexEmail" runat="server" ErrorMessage="Please enter a valid email" ControlToValidate="tbEmailAddr" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Text="*" ForeColor="Red"></asp:RegularExpressionValidator>
                        </div>

                        <!--Password-->
                        <div class="form-group-lg">
                            <label for="exampleInputPassword1">Password</label>
                            <asp:TextBox ID="tbPasswd" placeholder="Password" runat="server" CssClass="form-control form-control-lg" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqPwd" runat="server" ControlToValidate="tbPasswd" ErrorMessage="Pls enter an password" Text="*"></asp:RequiredFieldValidator>
                        </div>
                        <p>
                            <br>
                            I'm a...
                        </p>

                        <asp:RadioButtonList CssClass="rdoBtnStyle" ID="rdoMemberType" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                            <asp:ListItem>Student</asp:ListItem>
                            <asp:ListItem>Mentor</asp:ListItem>
                            <asp:ListItem>Admin</asp:ListItem>
                            <asp:ListItem>Parent</asp:ListItem>
                        </asp:RadioButtonList>
                       
                        <asp:Button ID="loginBtn" CssClass="btn btn-primary btn-block" runat="server" Text="Log in" OnClick="loginBtn_Click" />
                        <asp:Label ID="lblErrorMsg" runat="server"></asp:Label>

                         <a href="Private/Parent/registerParent.aspx">Register as a Parent</a>


                        <asp:ValidationSummary ID="errorSum" runat="server" DisplayMode="List" HeaderText="Please correct the following:" />
                    </form>
                    
                </div>
            </div>
        </div>
    </div>


</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace webAssignmentR
{
    public partial class studentPortfolio : System.Web.UI.Page
    {


        protected void Page_Load(object sender, EventArgs e)
        {
                displayStudents();
        }

        private void displayStudents()
        {
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolioConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);

            SqlCommand cmd = new SqlCommand("SELECT * FROM Student", conn);
            SqlDataAdapter daStudent = new SqlDataAdapter(cmd);
            DataSet res = new DataSet();
            conn.Open();
            daStudent.Fill(res, "StudentVals");
            conn.Close();
            gvStudent.DataSource = res.Tables["StudentVals"];
            gvStudent.DataBind();



        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string name = txtSearch.Text;
            if (name == "")
            {
                string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolioConnectionString"].ToString();
                SqlConnection conn = new SqlConnection(strConn);

                SqlCommand cmd = new SqlCommand("SELECT * FROM Student", conn);
                SqlDataAdapter daStudent = new SqlDataAdapter(cmd);
                DataSet res = new DataSet();
                conn.Open();
                daStudent.Fill(res, "StudentVals");
                conn.Close();
                gvStudent.DataSource = res.Tables["StudentVals"];
                gvStudent.DataBind();
            }
            else
            {
                string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolioConnectionString"].ToString();
                SqlConnection conn = new SqlConnection(strConn);
                SqlCommand cmd = new SqlCommand("SELECT * FROM Student WHERE Name =@name ", conn);
                cmd.Parameters.AddWithValue("@name", name);
                SqlDataAdapter daStudent = new SqlDataAdapter(cmd);
                DataSet res = new DataSet();
                conn.Open();
                daStudent.Fill(res, "StudentVals");
                conn.Close();
                gvStudent.DataSource = res.Tables["StudentVals"];
                gvStudent.DataBind();
            }
        }


        protected void gvStudent_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvStudent.PageIndex = e.NewPageIndex;
            displayStudents();
        }
    }
}
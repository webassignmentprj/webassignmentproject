﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="navBar.ascx.cs" Inherits="webAssignmentR.navBar" %>
<link href="styles/navBar.css" rel="stylesheet" />

<nav class="navbar fixed-bottom navbar-expand-md navbar-light bg-light  py-0">
    <a class="navbar-brand" href="login.aspx">
        <span>
            <img src="Images/image3.png" alt="infinityLogo">
        </span>
    </a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav w-100 customNav">
            <li class="nav-item">
                <a class="nav-link" href="studentPortfolio.aspx">Student Portfolio</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="projects.aspx">Projects</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="about.aspx">About</a>
            </li>
            <li class="nav-item">
                <a class="nav-link loginBtn" href="login.aspx">Login</a>
            </li>
        </ul>
    </div>

</nav>

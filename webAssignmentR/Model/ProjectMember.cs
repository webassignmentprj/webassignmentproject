﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace webAssignmentR.Model
{
    public class ProjectMember
    {
        public int projectID { get; set; }
        public int studentID { get; set; }
        public string role { get; set; }
        public string reflect { get; set; }

        public void addProjectMembers()
        {
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolioConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);       
                SqlCommand command = new SqlCommand("INSERT INTO ProjectMember(ProjectID,StudentID,Role)VALUES(@projid,@stuid,@role)", conn);
                command.Parameters.AddWithValue("@projid", projectID);
                command.Parameters.AddWithValue("@stuid", studentID);
            command.Parameters.AddWithValue("@role", role);
            conn.Open();
            command.ExecuteNonQuery();
            conn.Close();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace webAssignmentR.Model
{
    public class Student
    {
        public int studentID { get; set; }
        public string name { get; set; }
        public string course { get; set; }
        public string photo { get; set; }
        public string desc { get; set; }
        public string achieve { get; set; }
        public string link { get; set; }
        public string email { get; set; }
        public string pass { get; set; }
        public string status { get; set; }
        public int mentorID { get; set; }
        public string skills { get; set; } //skill ID OF SELECTED SKILLS
        

        public int GetSkills()
        {
            DataSet res = DatabaseProxy.Shared.selectCmd("SkillSet", "SELECT SkillSetName " +
                "FROM SkillSet ");
            /*"INNER JOIN StudentSkillSet " +
            "ON SkillSet.SkillSetID = StudentSkillSet.SkillSetID " +
            $"AND StudentSkillSet.StudentID = {studentID}");*/
            if (res.Tables["SkillSet"].Rows.Count > 0)
            {
                DataTable table = res.Tables["SkillSet"];
                int num = res.Tables["SkillSet"].Rows.Count;
                for (int i = 0; i < num; i++) {
                    if (!DBNull.Value.Equals(table.Rows[i]["SkillSetName"]))
                    {
                        if (!(num - i == 1))
                        {
                            skills += table.Rows[i]["SkillSetName"].ToString() + ",";
                        }
                        else
                        {
                            skills += table.Rows[i]["SkillSetName"].ToString();
                        }
                    }
                }
                return 0;
            }
            else
            {
                return 1;
            }
        }
        /*public int currentSkills()
        {
            DataSet res = DatabaseProxy.Shared.selectCmd("SkillSet", "SELECT SkillSetName " +
                "FROM SkillSet " +
            "INNER JOIN StudentSkillSet " +
            "ON SkillSet.SkillSetID = StudentSkillSet.SkillSetID " +
            $"AND StudentSkillSet.StudentID = {studentID}");
            if (res.Tables["SkillSet"].Rows.Count > 0)
            {
                DataTable table = res.Tables["SkillSet"];
                int num = res.Tables["SkillSet"].Rows.Count;
                for (int i = 0; i < num; i++)
                {
                    if (!DBNull.Value.Equals(table.Rows[i]["SkillSetName"]))
                    {
                        if (!(num - i == 1))
                        {
                            skillSet += table.Rows[i]["SkillSetName"].ToString() + ",";
                        }
                        else
                        {
                            skillSet += table.Rows[i]["SkillSetName"].ToString();
                        }
                    }
                }
                return 0;
            }
            else
            {
                return 1;
            }
        }*/

        public int GetDescAchievement()
        {
            DataSet res = DatabaseProxy.Shared.selectCmd("Student", "SELECT * FROM Student " +
                $"WHERE Student.StudentID = {studentID}");
            if (res.Tables["Student"].Rows.Count > 0)
            {
                DataTable table = res.Tables["Student"];
                if (!DBNull.Value.Equals(table.Rows[0]["Description"]))
                {
                    desc = table.Rows[0]["Description"].ToString();
                }
                if (!DBNull.Value.Equals(table.Rows[0]["Achievement"]))
                {
                    achieve = table.Rows[0]["Achievement"].ToString();
                }
                if (!DBNull.Value.Equals(table.Rows[0]["Photo"]))
                {
                    photo = table.Rows[0]["Photo"].ToString();
                }
                if (!DBNull.Value.Equals(table.Rows[0]["EmailAddr"]))
                {
                    email = table.Rows[0]["EmailAddr"].ToString();
                }
                if (!DBNull.Value.Equals(table.Rows[0]["ExternalLink"]))
                {
                    link = table.Rows[0]["ExternalLink"].ToString();
                }
            }
            return 0;
        }

        public int updateDescAchieve()
        {
            /*DatabaseProxy.Shared.nonQueryCmd("Student", "UPDATE Student SET " +
                $"Description = '{desc}' " +
                $"Achievement = '{achieve}' WHERE StudentID = {studentID}");

            return 0;*/
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolioConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("UPDATE Student SET " +
                "Description = @desc," +
                "Achievement = @achieve, ExternalLink =@link, Photo =@photo, EmailAddr =@email WHERE StudentID = @studentID", conn);
            cmd.Parameters.AddWithValue("@desc", desc);
            cmd.Parameters.AddWithValue("@achieve", achieve);
            cmd.Parameters.AddWithValue("@studentID", studentID);
            cmd.Parameters.AddWithValue("@link", link);
            cmd.Parameters.AddWithValue("@photo",photo);
            cmd.Parameters.AddWithValue("@email",email);
            conn.Open();
            int count = cmd.ExecuteNonQuery();
            conn.Close();
            if (count > 0)
                return 0;
            else
                return -2;
        }


        public int addStudentSkill()
        {
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolioConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("INSERT INTO StudentSkillSet(StudentID,SkillSetID)" +
                "VALUES" +
                "(@studentID,@skillSetID)", conn);
            cmd.Parameters.AddWithValue("@studentID", studentID);
            cmd.Parameters.AddWithValue("@skillSetID",skills);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            return 1;
        }
        public int deleteExisting()
        {
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolioConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("DELETE FROM StudentSkillSet WHERE StudentID =@studentID and SkillSetID =@skillSetID", conn);
            cmd.Parameters.AddWithValue("@studentID", studentID);
            cmd.Parameters.AddWithValue("@skillSetID", skills);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            return 1;
        }
    }
}
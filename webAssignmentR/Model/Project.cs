﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace webAssignmentR.Model
{
    public class Project
    {
        public int projectID { get; set; }
        public string title { get; set; }
        public string desc { get; set; }
        public string poster { get; set; }
        public string projectURL { get; set; }
        
        public void addProj()
        {
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolioConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("INSERT INTO Project ( Title, Description, ProjectPoster, ProjectURL)" +
                "VALUES(@title,@desc,@poster,@url)", conn);
            cmd.Parameters.AddWithValue("@title", title);
            cmd.Parameters.AddWithValue("@desc", desc);
            cmd.Parameters.AddWithValue("@poster", poster);
            cmd.Parameters.AddWithValue("@url", projectURL);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }
     
    }
    
}
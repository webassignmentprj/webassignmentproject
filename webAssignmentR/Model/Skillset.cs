﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace webAssignmentR
{
    public class Skillset
    {
        private int id;
        private string name;

        public int Id { get => id; }
        public string Name { get; set; }

        public Skillset(string _name) => name = _name;

        public bool Exists
        {
            get
            {
                DataSet res = DatabaseProxy.Shared.selectCmd("SkillSet", $"SELECT * FROM SkillSet WHERE SkillSet.SkillSetName ='{name}'");
                return res.Tables["SkillSet"].Rows.Count > 0;
            }
        }

    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Private/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="skillSets.aspx.cs" Inherits="webAssignmentR.Private.Admin.skillSets" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="allAdminPages.css" rel="stylesheet" />
    <div class="col-10 p-3">
        <div class="header">
            <h1>Edit Student
        <br />
                SkillSets</h1>
            <p>Add/Delete Skillsets</p>
        </div>
        <div class="canvas container-fluid">
            <b>
                <p>
                    Skill Set Name
                </p>
            </b>
            <asp:TextBox ID="txtSkillSet" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtSkillSet" ErrorMessage="Please enter a valid skill set"></asp:RequiredFieldValidator>
            <br />
            <asp:Button ID="btnSubmit" runat="server" Text="Add" CssClass="btn-primary" BackColor="#1C0C4E" OnClick="btnSubmit_Click" />
        </div>
    </div>

</asp:Content>

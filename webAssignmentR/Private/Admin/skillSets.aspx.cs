﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace webAssignmentR.Private.Admin
{
    public partial class skillSets : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                txtSkillSet.Text = "";
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Skillset skillset = new Skillset(txtSkillSet.Text);
                if (skillset.Exists)
                {
                    Response.Write("<script> alert('Skill set already exists!');</script>");
                }
                else
                {
                    DatabaseProxy.Shared.nonQueryCmd("SkillSet", $"INSERT INTO SkillSet (SkillSetName) VALUES ('{txtSkillSet.Text}')");
                    Response.Write($"<script> alert('Successfully added new skillset {skillset.Name}!');</script>");
                }
            }
        }
    }
}
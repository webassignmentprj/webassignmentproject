﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace webAssignmentR.Private.Admin
{
    public partial class mentors : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

            }

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (!DatabaseProxy.Shared.Exists("EmailAddr", "'" +txtEmail.Text+ "'", "Mentor"))
                {
                    DatabaseProxy.Shared.nonQueryCmd("Mentor", $"INSERT INTO Mentor(Name, EmailAddr) VALUES('{txtName.Text}', '{txtEmail.Text}')");
                }
                else
                {
                    Response.Write($"<script>alert('Mentor with email {txtEmail.Text} already exists!'); </script>");
                }
            }
        }
    }
}
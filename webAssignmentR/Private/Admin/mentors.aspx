﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Private/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="mentors.aspx.cs" Inherits="webAssignmentR.Private.Admin.mentors" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="allAdminPages.css" rel="stylesheet" />
    <div class="header">
        <h1>Add
        <br />
            Mentors</h1>
        <p>Create Mentor accounts in database</p>
    </div>
    <div class="canvas container-fluid">
        <h4><b>Mentor Particulars</b></h4>
        <p><b>Name</b></p>
        <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName" ErrorMessage="Please enter name"></asp:RequiredFieldValidator>
        <p><b>Email</b></p>
        <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEmail" ErrorMessage="Please enter email"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail" ErrorMessage="Email invalid" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
        <br />
        <asp:Button ID="btnSubmit" runat="server" Text="Add" CssClass="btn-primary" BackColor="#1C0C4E" OnClick="btnSubmit_Click" />
    </div>



</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace webAssignmentR.Private.Admin
{
    public partial class AdminMaster : System.Web.UI.MasterPage 
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((string)Session["UserType"] == null) Response.Redirect("/Login.aspx");
            else if ((string)Session["UserType"] != "Admin")
            {
                string path = "/Private/";
                switch ((string)Session["UserType"])
                {
                    case "Admin":
                        path += "Admin/parentViewReq.aspx";
                        break;
                    case "Mentor":
                        path += "Mentor/viewStudProf.aspx";
                        break;
                    case "Student":
                        path += "Student/editProf.aspx";
                        break;
                    case "Parent":
                        path += "Parent/editProfile.aspx";
                        break;
                    default: break;
                }
                Response.Redirect(path);
            }
        }
    }
}
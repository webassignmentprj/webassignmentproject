﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace webAssignmentR
{
    // Singleton to help with reducing boilerplatey-ness (DRY!)
    public class DatabaseProxy
    {
        private const string connectionKey = "Student_EPortfolioConnectionString";

        private static DatabaseProxy instance;
        private DatabaseProxy() { }

        public static DatabaseProxy Shared
        {
            get
            {
                if (instance == null)
                {
                    instance = new DatabaseProxy();
                }

                return instance;
            }
        }

        private string connStr { get => ConfigurationManager.ConnectionStrings[connectionKey].ToString(); }

        // For manual operations
        public SqlConnection databaseConnection => new SqlConnection(connStr);

        // Helper for simple select 
        public DataSet selectCmd(string table, string sql, List<Tuple<string, string>> queryParams = null)
        {
            SqlConnection conn = databaseConnection;

            SqlCommand cmd = new SqlCommand(sql, conn);

            if (queryParams != null)
            {
                foreach (Tuple<string, string> kvpair in queryParams)
                {
                    cmd.Parameters.AddWithValue($"@{kvpair.Item1}", kvpair.Item2);
                }
            }

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            DataSet res = new DataSet();

            conn.Open();
            da.Fill(res, table);
            conn.Close();

            return res; 
        }
        

        public bool Exists(string attribute, string value_check, string table)
        {
            DataSet res = selectCmd(table, $"SELECT * FROM {table} WHERE {attribute} = {value_check}");
            return res.Tables[table].Rows.Count > 0;
        }

        // Helper for simple insert
        public void nonQueryCmd(string table, string sql, List<Tuple<string, string>> queryParams = null)
        {
            SqlConnection conn = databaseConnection;

            SqlCommand cmd = new SqlCommand(sql, conn);

            if (queryParams != null)
            {
                foreach (Tuple<string, string> kvpair in queryParams)
                {
                    cmd.Parameters.AddWithValue($"@{kvpair.Item1}", kvpair.Item2);
                }
            }

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

        }

    }
}
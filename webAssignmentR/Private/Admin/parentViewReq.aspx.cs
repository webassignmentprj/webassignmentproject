﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace webAssignmentR.Private.Admin
{
    public partial class parentViewReq : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                DataSet res = DatabaseProxy.Shared.selectCmd("ViewingRequest", "select * from ViewingRequest INNER JOIN Parent on ViewingRequest.ParentID = Parent.ParentID");
                DataTable dataTable = res.Tables["ViewingRequest"];
                gvViewRequest.DataSource = dataTable;
                gvViewRequest.DataBind();
                btnUpdateReview.Enabled = false;
                ddlStatus.Enabled = false;
                btnUpdateReview.Text = "-";
                int rowsCount = dataTable.Rows.Count;
                lblReq.Text = rowsCount > 0 ? $"{rowsCount} Requests" : "No Requests";
            }


        }

        // Switched selected index 
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnUpdateReview.Enabled = true;
            ddlStatus.Enabled = true; 
            btnUpdateReview.Text = "Update";
            txtSelectedParent.Text = (string)gvViewRequest.SelectedDataKey["ParentName"];
            txtSelectedStudent.Text = (string)gvViewRequest.SelectedDataKey["StudentName"];
            txtReviewID.Text = Convert.ToString((int)gvViewRequest.SelectedDataKey["ViewingRequestID"]);
            ddlStatus.SelectedIndex = new List<string> { "P", "A", "R" }.IndexOf((string)gvViewRequest.SelectedDataKey["Status"]);
        }

        protected void btnUpdateReview_Click(object sender, EventArgs e)
        {
            DatabaseProxy.Shared.nonQueryCmd("ViewingRequest", $"UPDATE ViewingRequest SET Status = '{ddlStatus.SelectedValue}' WHERE ViewingRequestID = ${(int)gvViewRequest.SelectedDataKey["ViewingRequestID"]}");
            Response.Redirect("parentViewReq.aspx");
        }
    }
}
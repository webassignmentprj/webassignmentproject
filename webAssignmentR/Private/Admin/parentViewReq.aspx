﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Private/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="parentViewReq.aspx.cs" Inherits="webAssignmentR.Private.Admin.parentViewReq" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="allAdminPages.css" rel="stylesheet" />
    <div class="header">
        <h1>Parent View
        <br />
            Requests
        </h1>
        <br />
        <p>View and accept view request from parents</p>
        <div class="canvas container-fluid">
            <asp:Label ID="lblReq" runat="server" Text="XX Requests"></asp:Label>
        </div>


        <asp:GridView CssClass="w-100 m-3" ID="gvViewRequest" runat="server" AllowPaging="True" AutoGenerateColumns="False" AutoGenerateSelectButton="True" CellPadding="15" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" PageSize="5" DataKeyNames="ParentName,StudentName,Status,ViewingRequestID">
            <AlternatingRowStyle BackColor="White" BorderStyle="None" />
            <Columns>
                <asp:BoundField DataField="ViewingRequestID" HeaderText="View Request ID" />
                <asp:BoundField DataField="ParentName" HeaderText="ParentName" />
                <asp:BoundField DataField="ParentID" HeaderText="ParentID" />
                <asp:BoundField DataField="StudentName" HeaderText="StudentName" />
                <asp:BoundField DataField="StudentID" HeaderText="StudentID" />
                <asp:BoundField DataField="Status" HeaderText="Status" />
            </Columns>
            <EmptyDataTemplate>
                No Data<br />
            </EmptyDataTemplate>
        </asp:GridView>


        <div class="canvas container-fluid">
            <h3><b>Review View Request</b></h3>
            <p>
            <b>Parent: </b>
            <asp:Label ID="txtSelectedParent" runat="server" Text="-"></asp:Label><br />
            </p>
            <p>
            <b>Student: </b>
            <asp:Label ID="txtSelectedStudent" runat="server" Text="-"></asp:Label><br />
            </p>
            <p>
                        <b>Viewing Req. ID: </b>
            <asp:Label ID="txtReviewID" runat="server" Text="-"></asp:Label> <br />
            </p>
            <p>
                        <b>Status: </b>
                        <asp:DropDownList ID="ddlStatus" runat="server">
                            <asp:ListItem Value="P">Pending</asp:ListItem>
                            <asp:ListItem Value="A">Approved</asp:ListItem>
                            <asp:ListItem Value="R">Rejected</asp:ListItem>
                        </asp:DropDownList><br />
            </p>
            <br />
            <asp:Button CssClass="btn-primary" ID="btnUpdateReview" runat="server" Text="Update" OnClick="btnUpdateReview_Click" />

        </div>
    </div>
</asp:Content>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="adminNavbar.ascx.cs" Inherits="webAssignmentR.Private.Admin.adminNavbar" %>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
    crossorigin="anonymous">
<link href="adminNav.css" rel="stylesheet" />
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="adminNav.js"></script>


<div id="nav" class="side-bar border-right d-xl-none d-lg-none d-md-none" style="width: 0px;">
    <div id="topSection">
        <img src="../../Images/image3.png" / >
        <h1>System Admin Mode</h1 >
    </div >

    <ul >
        <li class="nav-item" >
            <a class="nav-link " href="parentViewReq.aspx" > Parent View Requests</a >
        </li >
        <li class="nav-item" >
            <a class="nav-link " href="skillSets.aspx" > Skill Sets</a >
        </li >
        <li class="nav-item" >
            <a class="nav-link " href="students.aspx" > Students</a >
        </li >
        <li class="nav-item" >
            <a class="nav-link " href="mentors.aspx" > Mentor</a >
        </li >
        <li class="nav-item" >
            <asp:Button ID="btnLogOut" Text="Log Out" CssClass="btn btn-link nav-link"  style="font-size:30px" runat="server"  CausesValidation="False" OnClick="btnLogOut_Click" />
        </li >
    </ul >
</div>
            <a class="btn d-md-none d-lg-none d-sm-block d-xl-none m-3 col-1"></a>

<div class="side-bar border-right d-md-block d-lg-block d-sm-none d-none col-lg-2 col-md-3">
    <div id="topSection">
        <img src="../../Images/image3.png" / >
        <h1>System Admin Mode</h1 >
    </div >

    <ul >
        <li class="nav-item" >
            <a class="nav-link " href="parentViewReq.aspx" > Parent View Requests</a >
        </li >
        <li class="nav-item" >
            <a class="nav-link " href="skillSets.aspx" > Skill Sets</a >
        </li >
        <li class="nav-item" >
            <a class="nav-link " href="students.aspx" > Students</a >
        </li >
        <li class="nav-item" >
            <a class="nav-link " href="mentors.aspx" > Mentor</a >
        </li >
        <li class="nav-item" >
            <asp:Button ID="Button1" Text="Log Out" CssClass="btn btn-link nav-link"  style="font-size:30px" runat="server"  CausesValidation="False" OnClick="btnLogOut_Click" />
        </li >
    </ul >
</div>


 <script>
     let opened = false;
     $('.btn').on("click", function () {
         $('.btn').toggleClass('btnc');
         document.getElementById('nav').style.width = opened ? '0px' : '80%';
         opened = !opened;
     });
</script>
            






﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace webAssignmentR.Private.Admin
{
    public partial class adminNavbar : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Session.Abandon();  //deletes all user stored in the session
            Response.Redirect("~/login.aspx");//redirect to start page
             
        }
    }
}
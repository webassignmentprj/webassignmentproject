﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace webAssignmentR.Private.Admin
{
    public partial class students : System.Web.UI.Page
    {
        static string filename;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                DataSet parents = DatabaseProxy.Shared.selectCmd("Mentor", "SELECT * FROM Mentor");
                gvMentorList.DataSource = parents.Tables["Mentor"];
                gvMentorList.DataBind();
                btnSubmit.Enabled = false;
            }
        }

        // Upload profile picture
        private string uploadPicture()
        {
            // Check image type and size
            if (FileUploadProfilePicture.HasFile && FileUploadProfilePicture.PostedFile.ContentType == "image/jpeg" && FileUploadProfilePicture.PostedFile.ContentLength < 1000000)
            {
                try
                {
                    string filename = Path.GetFileName(FileUploadProfilePicture.FileName);
                    FileUploadProfilePicture.SaveAs(Server.MapPath("/Images/Students/") + filename);
                    Response.Write("<script>alert('Upload success!');</script>");
                    return filename;
                }
                catch (Exception ex)
                {
                    Response.Write("<script>alert('Upload failed!');</script>");
                    return "default.jpg";
                }
            }
            return "default.jpg";
        }


        protected void gvMentorList_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnSubmit.Enabled = true;
            lblSelectedParent.Text = $"Assigned Mentor {gvMentorList.SelectedDataKey["Name"]} (ID: {gvMentorList.SelectedDataKey["MentorID"]})";

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            // Upload image
            filename = uploadPicture();

            DataSet res = DatabaseProxy.Shared.selectCmd("Student", $"SELECT * FROM Student WHERE EmailAddr = '{txtEmail.Text}'");

            bool exists = (res.Tables["Student"].Rows.Count > 0);
            if (exists)
            {
                Response.Write($"<script>alert('Student with email {txtEmail.Text} already exists!');</script>");
            }
            else
            {
                if (Page.IsValid)
                {
                    DatabaseProxy.Shared.nonQueryCmd("Student",
                        "INSERT INTO Student(Name, Course, Photo, Description, Achievement, ExternalLink, EmailAddr, MentorID) " +
                        $"VALUES('{txtName.Text}', '{txtCourse.Text}', '{filename}', '{txtDescription.Text}','{txtProfile.Text}', '{txtProfileLink.Text}', '{txtEmail.Text}', {gvMentorList.SelectedDataKey["MentorID"]})"
                        );
                }
                else
                {
                    Response.Write("<script>alert('Error!');</script>");
                }
            }
        }

    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Private/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="students.aspx.cs" Inherits="webAssignmentR.Private.Admin.students" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="allAdminPages.css" rel="stylesheet" />
    <div class="header">
        <h1>Add
        <br />
            Students</h1>
        <p>Create Students in database</p>
    </div>
    <div class="canvas container-fluid">
        <div class="row">
            <div class="col-lg-7 col-md-12 border-bottom  border-right">
                <br />
                <h4>
                    <b>Assign a mentor</b>
                </h4>
                <asp:GridView ID="gvMentorList" runat="server" AutoGenerateColumns="False" AutoGenerateSelectButton="True" BorderStyle="None" OnSelectedIndexChanged="gvMentorList_SelectedIndexChanged" DataKeyNames="MentorID,Name">
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:BoundField DataField="MentorID" HeaderText="Mentor ID" />
                        <asp:BoundField DataField="Name" HeaderText="Mentor Name" />
                    </Columns>
                </asp:GridView>
                <br />
                <asp:Label ID="lblSelectedParent" runat="server" Text="-"></asp:Label><br />
                <br />
                <h4>
                    <b>Add a profile picture</b>
                </h4>
                <br />
                <asp:FileUpload ID="FileUploadProfilePicture" runat="server" /><br />
                <br />
                <p><b>Profile link</b></p>
                <asp:TextBox ID="txtProfileLink" runat="server"></asp:TextBox>
            </div>
            <div class="col-lg-5 col-md-12 border-left border-right border-bottom">
                <br />
                <h4>
                    <b>Student Particulars</b>
                </h4>
                <p><b>Name</b></p>
                <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName" ErrorMessage="Please provide student name"></asp:RequiredFieldValidator>
                <p><b>Email</b></p>
                <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox><br />
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail" ErrorMessage="Please enter valid email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEmail" ErrorMessage="Please provide student email"></asp:RequiredFieldValidator>
                <p><b>Course</b></p>
                <asp:TextBox ID="txtCourse" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtCourse" ErrorMessage="Please provide student course"></asp:RequiredFieldValidator>
                <p><b>Profile</b></p>
                <asp:TextBox ID="txtProfile" runat="server" Height="39px"></asp:TextBox>
                <p><b>Description</b></p>
                <asp:TextBox ID="txtDescription" runat="server" Height="50px"></asp:TextBox>

                <asp:Button runat="server" Text="Create" CssClass="btn-primary" Style="background-color: #302384" ID="btnSubmit" OnClick="btnSubmit_Click" />

            </div>

        </div>
    </div>


</asp:Content>

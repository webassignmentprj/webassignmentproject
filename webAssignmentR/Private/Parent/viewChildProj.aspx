﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Private/Parent/parentMaster.Master" AutoEventWireup="true" CodeBehind="viewChildProj.aspx.cs" Inherits="webAssignmentR.Private.Parent.viewChildProj" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="allParentPages.css" rel="stylesheet" />
    <div class="row">
        <div class="flex-column">


            <h1>View Child's project</h1>
            <p>
                <asp:GridView ID="gvProfile" runat="server" CssClass="col-lg-12 col-md-10 col-sm-8" AutoGenerateColumns="False" Height="516px" Width="841px">
                    <Columns>
                        <asp:BoundField DataField="Name" HeaderText="Name" />
                        <asp:BoundField DataField="Course" HeaderText="Course" />
                        <asp:BoundField DataField="Description" HeaderText="Description" />
                        <asp:BoundField DataField="Achievement" HeaderText="Achievement" />
                        <asp:BoundField DataField="External Link" HeaderText="External Link" />
                    </Columns>
                </asp:GridView>
            </p>
            <h1>Project's</h1>
            <p>
                <asp:GridView ID="gvProject" runat="server"   CssClass="col-lg-12 col-md-10 col-sm-8" AutoGenerateColumns="False" Height="562px" Width="829px">
                    <Columns>
                        <asp:BoundField DataField="Title" HeaderText="Title" />
                        <asp:BoundField DataField="Description" HeaderText="Description" />
                        <asp:BoundField DataField="ProjectPoster" HeaderText="Poster" />
                        <asp:BoundField DataField="ProjectURL" HeaderText="Project URL" />
                    </Columns>
                </asp:GridView>
            </p>
        </div>
    </div>
</asp:Content>

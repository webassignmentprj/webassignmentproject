﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Private/Parent/parentMaster.Master" AutoEventWireup="true" CodeBehind="viewReply.aspx.cs" Inherits="webAssignmentR.Private.Parent.viewReply" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid">
        <div class="row">

                <asp:GridView ID="gvMessage" runat="server" CssClass="col-lg-12 col-md-8 col-sm-6" AutoGenerateColumns="False" Width="1101px">
                    <Columns>
                        <asp:BoundField DataField="DateTimePosted" HeaderText="Date Time" />
                        <asp:BoundField DataField="Name" HeaderText="Mentor Name" />
                        <asp:BoundField DataField="Text" HeaderText="Message" />
                    </Columns>
                </asp:GridView>
        </div>
    </div>
</asp:Content>

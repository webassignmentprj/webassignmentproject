﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace webAssignmentR.Private.Parent
{
    public partial class viewChildProj : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            showStudent();
            showProject();
        }

        public void showStudent()
        {
            int parentid = Convert.ToInt16(Session["UserID"]);
            DataSet show= DatabaseProxy.Shared.selectCmd("Project", $"select name, course, Description, Achievement, ExternalLink from student inner join ViewingRequest on student.StudentID = ViewingRequest.StudentID where ViewingRequest.ParentID ={parentid}");
            gvProfile.DataSource = show.Tables["Student"];
            gvProfile.DataBind();

        }

        public void showProject()
        {
            int parentid = Convert.ToInt16(Session["UserID"]);
            DataSet project = DatabaseProxy.Shared.selectCmd("ProjectMember", $"select project.Title, project.Description, project.ProjectPoster, ProjectURL from ProjectMember inner join student on ProjectMember.StudentID = student.StudentID inner join ViewingRequest on student.StudentID = ViewingRequest.StudentID inner join project on project.ProjectID = ProjectMember.ProjectID where ViewingRequest.ParentID={parentid}");
            gvProject.DataSource = project.Tables["ProjectMember"];
            gvProject.DataBind();
    }
    } 

    
   
}
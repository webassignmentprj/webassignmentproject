﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Private/Parent/parentMaster.Master" AutoEventWireup="true" CodeBehind="requestStudent.aspx.cs" Inherits="webAssignmentR.Private.Parent.requestStudent" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <asp:GridView ID="gvRequest" runat="server" AutoGenerateColumns="False" OnRowCommand="gv_Selected">
        <Columns>
            <asp:BoundField DataField="StudentName" HeaderText="Student Name" />
            <asp:BoundField DataField="ParentName" HeaderText="Parent Name" />
            <asp:ButtonField Text="Select" />
        </Columns>
    </asp:GridView>
    <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
</asp:Content>

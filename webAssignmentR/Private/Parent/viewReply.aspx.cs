﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace webAssignmentR.Private.Parent
{
    public partial class viewReply : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Reply();
        }

        public void Reply()
        {
            DataSet res = DatabaseProxy.Shared.selectCmd("Reply", $"select mentor.Name,DateTimePosted,Text from reply inner join mentor on reply.MentorID=mentor.mentorid");
            gvMessage.DataSource = res.Tables["Reply"];
            gvMessage.DataBind();
        }
    }
}
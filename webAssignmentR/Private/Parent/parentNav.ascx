﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="parentNav.ascx.cs" Inherits="webAssignmentR.Private.Parent.parentNav" %>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
    crossorigin="anonymous">
<link href="parentNav.css" rel="stylesheet" />
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="parentNav.js"></script>


<div class="sideBar" >
    <div id="topSection" >
        <img src="../../Images/image3.png" / >
        <h1>Parent Mode</h1 >
    </div >

    <ul >
        <li class="nav-item" >
            <a class="nav-link " href="requestStudent.aspx" >Request to view Student</a >
        </li >
        <li class="nav-item" >
            <a class="nav-link " href="viewChildProj.aspx" > View Children Project</a >
        </li >
        <li class="nav-item" >
            <a class="nav-link " href="msgMentor.aspx" > Message Mentor</a >
        </li >
        <li class="nav-item" >
            <a class="nav-link " href="viewReply.aspx" > View Reply</a >
        </li >
        <li class="nav-item" >
            <asp:Button ID="btnLogOut" Text="Log Out" CssClass="btn btn-link nav-link"  style="font-size:30px" runat="server"  CausesValidation="False" OnClick="btnLogOut_Click" />
        </li >
       
    </ul >
</div>
<a class="btn"></a>

<script>
    $('.btn').on("click", function () {
    $('.btn').toggleClass('btnc');
    $('.sideBar').toggleClass('side');
})
</script>

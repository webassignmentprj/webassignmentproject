﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace webAssignmentR.Private.Parent
{
    public partial class msgMentor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!Page.IsPostBack)
            {
                DataSet res = DatabaseProxy.Shared.selectCmd("Mentor", $"select Name,Mentorid from mentor");
                ddlMentor.DataSource = res.Tables["Mentor"];
                ddlMentor.DataTextField = "Name";
                ddlMentor.DataValueField = "Name";
                ddlMentor.DataBind();

                
            }
            
        }

        //protected void ddlMentor_SelectedIndexChanged(object sender, EventArgs e)
        //{
           // int mentorid = ddlMentor.SelectedIndex + 1;
        //}

        protected void btnSend_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                int parentid = Convert.ToInt16(Session["UserID"]);

                DataSet res = DatabaseProxy.Shared.selectCmd("Mentor", $"Select MentorID from mentor where name='{ddlMentor.SelectedValue}'");
                int toID = (int) res.Tables["Mentor"].Rows[0]["MentorID"];
                DatabaseProxy.Shared.nonQueryCmd("Reply", $"INSERT INTO Message (FromID,ToID,Title,Text) Values ({parentid}, {toID}, '{txtTitle.Text}','{txtMessage.Text}')");
            }

        }

        protected void dropdownlist()
        {
            DataSet res = DatabaseProxy.Shared.selectCmd("Mentor", $"select Name,Mentorid from mentor");
            ddlMentor.DataSource = res.Tables["Mentor"];
            ddlMentor.DataTextField = "Name";
            ddlMentor.DataValueField = "Name";
            ddlMentor.DataBind();
           
            
            
            
            

        }

       
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace webAssignmentR.Private.Parent
{
    public partial class requestStudent : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
             
            select();
            
        }

        private void select()
        {
            int parentid = Convert.ToInt16(Session["UserID"]);
            DataSet res = DatabaseProxy.Shared.selectCmd("Student", $"select StudentName,Parent.ParentName from ViewingRequest INNER JOIN Parent ON ViewingRequest.ParentID=Parent.ParentID where ViewingRequest.ParentID={parentid}");
            gvRequest.DataSource = res.Tables["Student"];
            gvRequest.DataBind();
            
        }

        protected void gv_Selected(object sender, GridViewCommandEventArgs e)

        {
            int parentid = Convert.ToInt16(Session["UserID"]);
            DatabaseProxy.Shared.nonQueryCmd("Student", $"update ViewingRequest Set Status = 'R' where ParentID={parentid}");
            
        }
    }
}
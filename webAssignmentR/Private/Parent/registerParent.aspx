﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Private/Parent/parentMaster.Master" AutoEventWireup="true" CodeBehind="registerParent.aspx.cs" Inherits="webAssignmentR.Private.Parent.registerParent" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%; background-color: #F6F0FF">
        <tr>
            <td style="width: 276px">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 276px">Parent Name:</td>
            <td>
                <asp:TextBox ID="txtParentName" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvParentName" runat="server" ControlToValidate="txtParentName" ErrorMessage="Please enter your name!">*</asp:RequiredFieldValidator>
                <asp:Label ID="lblExist" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 276px">Email :</td>
            <td>
                <asp:TextBox ID="txtEmailAddr" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmailAddr" ErrorMessage="Please enter email!">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmailAddr" ErrorMessage="Please enter a valid Email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td style="width: 276px">Password:</td>
            <td>
                <asp:TextBox ID="txtPassword" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtPassword" ErrorMessage="Password Required!">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revMinimumLength" runat="server" ControlToValidate="txtPassword" ErrorMessage="Minimum 8 Characters!" ValidationExpression=".{8,}">*</asp:RegularExpressionValidator>
                <asp:Button ID="btnConfirmation" runat="server" OnClick="btnConfirmation_Click" Text="Confirm" />
            </td>
        </tr>
        <tr>
            <td style="width: 276px">&nbsp;</td>
            <td>
                <asp:ValidationSummary ID="ValidationSummary" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace webAssignmentR.Private.Parent
{
    public partial class registerParent : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnConfirmation_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Parents objParent = new Parents();
                objParent.ParentName = txtParentName.Text;
                objParent.EmailAddr = txtEmailAddr.Text;
                objParent.Password = txtPassword.Text;

                if (!objParent.Exists)
                {
                    objParent.add();
                    Response.Redirect("~/login.aspx");
                }
                else
                {
                    lblExist.Text = "Email Exist!";
                }
                
                
            }
        }
    }
}
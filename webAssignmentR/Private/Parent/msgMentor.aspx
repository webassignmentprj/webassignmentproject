﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Private/Parent/parentMaster.Master" AutoEventWireup="true" CodeBehind="msgMentor.aspx.cs" Inherits="webAssignmentR.Private.Parent.msgMentor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="allParentPages.css" rel="stylesheet" />

    <div class="container-fluid">
        <div class="row">

            <h1>Message Mentor</h1>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <table class="w-100">
                <tr>
                    <td style="width: 201px">To:</td>
                    <td>
                        <asp:DropDownList ID="ddlMentor" runat="server" AutoPostBack="true">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlMentor" ErrorMessage="Please select a mentor" ForeColor="Red">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="width: 201px">Title:</td>
                    <td>
                        <asp:TextBox ID="txtTitle" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtTitle" ErrorMessage="Enter a Title please" ForeColor="Red">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="width: 201px">Message</td>
                    <td>
                        <asp:TextBox ID="txtMessage" runat="server" Height="241px" Width="549px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtMessage" ErrorMessage="Type something in the body" ForeColor="Red">*</asp:RequiredFieldValidator>
                        <br />
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" />
                        <br />
                        &nbsp;<asp:Button ID="btnSend" runat="server" OnClick="btnSend_Click" Text="Send" AutoPostBack="true" />
                        <asp:Label ID="Label1" runat="server" Text="" AutoPostBack="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 201px">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>

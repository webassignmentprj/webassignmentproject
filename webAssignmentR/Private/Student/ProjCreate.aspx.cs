﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;


namespace webAssignmentR.Private.Student
{
    
    public partial class ProjCreate : System.Web.UI.Page
    {
        static int selectedrow = 0;
        static int projId = 0;
        static List<Model.Student> memberlist = new List<Model.Student>();
        static int studentid = 0 ;
        string studeID;
        string studename;
        string projectname;
        bool repeatedname = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Username"] != null)
            {
                studentid = 0;
                studeID = (string)Session["UserID"];
                studename = (string)Session["Username"];
                studentid = Convert.ToInt32(studeID);

                if (!Page.IsPostBack)
                {
                    memberlist.Clear();
                    selectedrow = 0;
                }
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }

        }
        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            projectname = txtName.Text;
            getAllProjName();
            if(repeatedname == true)
            {
                lblCheckName.Text = "Title already in use! Please change title.";
            }
            if(Page.IsValid & repeatedname == false)
            {
                int count = 0;
                for(int i = 0; i < memberlist.Count; i++)
                {
                    if (Convert.ToInt32(studeID) == memberlist[i].studentID)
                    {
                        count += 1;
                    }
                }
                if (count > 0)
                {
                    storingData();
                    storeProjMember();
                    Response.Redirect("ConfirmCreate.aspx");
                    projId = 0;
                }
                else
                {
                    lblLeader.Text = "Please add Your Student ID!";
                }
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            addGroupMembers();         
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            string uploadedFile = "";
            if(upPhoto.HasFile == true)
            {
                string savepath;
                string fileExt = Path.GetExtension(upPhoto.FileName);
                uploadedFile = txtName.Text + fileExt;
                savepath = MapPath("../../Images/Projects/" + uploadedFile);
                try
                {
                    upPhoto.SaveAs(savepath);
                    lblMsg.Text = "File uploaded successfully";
                    imgPhoto.ImageUrl = "../../Images/Projects/" + uploadedFile;
   
                }
                catch(IOException)
                {
                    lblMsg.Text = "File uploading fail!";
                }
                catch(Exception ex)
                {
                    lblMsg.Text = ex.Message;
                }

            }
        }

        private void getAllProjName()
        {
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolioConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT Title FROM Project",conn);
            SqlDataAdapter daTitle = new SqlDataAdapter(cmd);
            DataSet res = new DataSet();
            conn.Open();
            daTitle.Fill(res, "projNames");
            conn.Close();
            int i = 0;
            foreach (DataRow dr in res.Tables["projNames"].Rows)
            {
                if (res.Tables["projNames"].Rows[i]["Title"].ToString() == projectname)
                {
                    repeatedname = true;
                }
                i += 1;
            }
        }
        private void addGroupMembers()
        {
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolioConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT StudentID, Name FROM Student WHERE StudentID = @studentID", conn);
            SqlCommand command = new SqlCommand("SELECT TOP 1 ProjectID FROM Project ORDER BY ProjectID DESC", conn);
            cmd.Parameters.AddWithValue("@studentID",Convert.ToInt32(txtID.Text));
            SqlDataAdapter daMember = new SqlDataAdapter(cmd);
            SqlDataAdapter daProjectId = new SqlDataAdapter(command);
            DataSet res = new DataSet();
            DataSet resul = new DataSet();
            conn.Open();
            daMember.Fill(res, "Projmember");
            daProjectId.Fill(resul, "ProjectID");
            conn.Close();
            int num = 0;
            if (memberlist.Count() > 0)
            {
                for (int i = 0; i < memberlist.Count(); i++)
                {
                    int id = Convert.ToInt32(txtID.Text);
                    if (id == memberlist[i].studentID)
                    {
                        num += 1;
                    }
                    if (i == memberlist.Count()-1 & num == 0)
                    {
                        Model.Student objstudent = new Model.Student();
                        objstudent.name = res.Tables["Projmember"].Rows[0]["Name"].ToString();
                        objstudent.studentID = Convert.ToInt32(res.Tables["Projmember"].Rows[0]["StudentID"]);
                        memberlist.Add(objstudent);
                        gvMemberList.DataSource = memberlist;
                        gvMemberList.DataBind();
                    };
                }
            }
            else
            {
                Model.Student leader = new Model.Student();
                leader.name = studename;
                leader.studentID = studentid;
                memberlist.Add(leader);
                int id = Convert.ToInt32(txtID.Text);
                if (studentid != id)
                {
                    Model.Student objstudent = new Model.Student();
                    objstudent.name = res.Tables["Projmember"].Rows[0]["Name"].ToString();
                    objstudent.studentID = Convert.ToInt32(res.Tables["Projmember"].Rows[0]["StudentID"]);
                    memberlist.Add(objstudent);
                    gvMemberList.DataSource = memberlist;
                    gvMemberList.DataBind();
                }
                else
                {
                    gvMemberList.DataSource = memberlist;
                    gvMemberList.DataBind();
                }
            }
            projId = Convert.ToInt32(resul.Tables["ProjectID"].Rows[0]["ProjectID"]);
        }

        private void storingData()
        {
            Model.Project objproj = new Model.Project();
            objproj.desc = txtDesc.Text;
            objproj.title = txtName.Text;
            objproj.poster = imgPhoto.ImageUrl.ToString();
            objproj.projectURL = txtUrl.Text;
            objproj.addProj();
        }
        private void storeProjMember()
        {
            for (int i = 0; i < memberlist.Count(); i++)
            {
                Model.ProjectMember objMember = new Model.ProjectMember();
                objMember.projectID = projId + 1;
                objMember.studentID = memberlist[i].studentID;
                if (studentid == memberlist[i].studentID)
                {
                    objMember.role = "Leader";
                }
                else
                {
                    objMember.role = "Member";
                }
                objMember.addProjectMembers();
            }
        }
        private void removeMember()
        {
            memberlist.RemoveAt(selectedrow);
        }
        protected void gvMemberList_RowCommand(object sender, GridViewCommandEventArgs e)
        {           
            selectedrow = Convert.ToInt32(e.CommandArgument);
            removeMember();
            gvMemberList.DataSource = memberlist;
            gvMemberList.DataBind();
        }

        protected void txtDesc_TextChanged(object sender, EventArgs e)
        {

        }

        protected void txtName_TextChanged(object sender, EventArgs e)
        {

        }

        protected void txtID_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
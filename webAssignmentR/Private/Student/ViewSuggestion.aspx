﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Private/Student/studentMaster.Master" AutoEventWireup="true" CodeBehind="ViewSuggestion.aspx.cs" Inherits="webAssignmentR.Private.Student.ViewSuggestion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="allStudentPages.css" rel="stylesheet" />
    <div class="container-fluid" align="center" style=" width:85%; overflow: auto;position: relative;">
    <h1 style="text-align:left; ">Suggestion - View</h1>
    <table cellpadding="20px" style=" width:100%; height:277px; background-color: #F6F0FF"  >
        <tr>
            <td align="center" style="height: 40px; " colspan="2">
                Created at :
                <asp:Label ID="lblDateTime" runat="server"></asp:Label>
&nbsp;by
                <asp:Label ID="lblMentor" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="height: 291px; text-align:center" colspan="2">
                <asp:TextBox ID="txtSuggestion" runat="server" Height="199px" Width="80%" ReadOnly="True" TextMode="MultiLine" ></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="height: 69px; width: 1557px"></td>
            <td style="height: 69px">
                <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="btnBack_Click" />
            </td>
        </tr>
    </table>
        </div>

</asp:Content>

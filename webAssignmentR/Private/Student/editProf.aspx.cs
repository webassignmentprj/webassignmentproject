﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace webAssignmentR.Private.Student
{
    public partial class editProf : System.Web.UI.Page
    {
        Model.Student objStudent = new Model.Student();
        
        protected void Page_Load(object sender, EventArgs e)
        {
                if (Session["Username"] != null)
                {
                    lblName.Text = (string)Session["Username"];
                    lblStudentID.Text = (string)Session["UserID"];
                    if (!Page.IsPostBack)
                    {
                        objStudent.studentID = Convert.ToInt32(lblStudentID.Text);
                        objStudent.GetSkills();
                        string skillz = objStudent.skills.ToString();
                        string[] skillname = skillz.Split(',');
                        cblSkills.DataSource = skillname;
                        cblSkills.DataBind();
                        objStudent.GetDescAchievement();
                        txtAchievement.Text = objStudent.achieve.ToString();
                        txtDesc.Text = objStudent.desc.ToString();
                        txtEmail.Text = objStudent.email.ToString();
                        txtHyperLink.Text = objStudent.link.ToString();
                        imgStudent.ImageUrl = "../../Images/Students/" + objStudent.photo;
                    }
                }
                else
                {
                    Response.Redirect("~/login.aspx");
                }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            List<int>skillInd = new List<int>();
            objStudent.studentID = Convert.ToInt32(lblStudentID.Text);
            objStudent.desc = txtDesc.Text;
            objStudent.achieve = txtAchievement.Text;
            objStudent.link = txtHyperLink.Text;
            objStudent.email = txtEmail.Text;
            objStudent.photo = imgStudent.ImageUrl.ToString();
            
            foreach(ListItem item in cblSkills.Items)
            {
                if (item.Selected)
                {
                    skillInd.Add(cblSkills.Items.IndexOf(item) + 1);
                }
            }
            for(int i = 0; i < skillInd.Count(); i++)
            {
                objStudent.skills = skillInd[i].ToString();
                objStudent.deleteExisting();
                objStudent.addStudentSkill();
            }
            objStudent.updateDescAchieve();
            Response.Redirect("EditProfConfirm.aspx");           
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            string uploadedFile = "";
            if (upPhoto.HasFile == true)
            {
                string savepath;
                string fileExt = Path.GetExtension(upPhoto.FileName);
                uploadedFile = lblName.Text + fileExt;
                savepath = MapPath("../../Images/Students/" + uploadedFile);
                try
                {
                    upPhoto.SaveAs(savepath);
                    lblMsg.Text = "File uploaded successfully";
                    imgStudent.ImageUrl = "../../Images/Students/" + uploadedFile;

                }
                catch (IOException)
                {
                    lblMsg.Text = "File uploading fail!";
                }
                catch (Exception ex)
                {
                    lblMsg.Text = ex.Message;
                }

            }
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Private/Student/studentMaster.Master" AutoEventWireup="true" CodeBehind="suggestionFrmMentor.aspx.cs" Inherits="webAssignmentR.Private.Student.suggestionFrmMentor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="allStudentPages.css" rel="stylesheet" />
    <h1>Suggestion</h1>
    <table cellpadding="10px" style=" width:85%; height:277px; background-color: #F6F0FF"  >
        <tr align="center" >
            <td align="center" >
                <asp:GridView ID="gvNames" runat="server" style="margin-left: 0px" Width="90%" AutoGenerateColumns="False" Height="80%" BackColor="White" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" CellPadding="2" OnRowCommand="gvNames_RowCommand">
                    <Columns>
                        <asp:BoundField DataField="name" HeaderText="Suggestion From:" />
                        <asp:ButtonField CommandName="View" Text="View" />
                    </Columns>
                    <EmptyDataTemplate>
                        No Suggestions.
                    </EmptyDataTemplate>
                    <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                    <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
                    <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                    <RowStyle BackColor="White" ForeColor="#003399" />
                    <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                    <SortedAscendingCellStyle BackColor="#EDF6F6" />
                    <SortedAscendingHeaderStyle BackColor="#0D4AC4" />
                    <SortedDescendingCellStyle BackColor="#D6DFDF" />
                    <SortedDescendingHeaderStyle BackColor="#002876" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>

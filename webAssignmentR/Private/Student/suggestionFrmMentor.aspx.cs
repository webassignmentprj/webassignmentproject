﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace webAssignmentR.Private.Student
{
    public partial class suggestionFrmMentor : System.Web.UI.Page
    {
        static int studentid;
        static int newid;
        static int suggestionID = 0;
        static List<Model.Student> namesList = new List<Model.Student>();
        protected void Page_Load(object sender, EventArgs e)
        {
                if (Session["Username"] != null)
                {
                    if (!Page.IsPostBack)
                    {
                    namesList.Clear();
                    string id = (string)Session["UserID"];
                    studentid = Convert.ToInt32(id);
                    getSuggestionName();
                }
            
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }
        }
        private void getSuggestionName()
        {
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolioConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT Name,Mentor.MentorID FROM Mentor INNER JOIN Suggestion ON Mentor.MentorID = Suggestion.MentorID WHERE Suggestion.StudentID =@studentID", conn);
            cmd.Parameters.AddWithValue("@studentID", studentid);
            SqlDataAdapter daMentor = new SqlDataAdapter(cmd);
            DataSet res = new DataSet();
            conn.Open();
            daMentor.Fill(res, "mentor");
            conn.Close();
            for(int i = 0;i<res.Tables["mentor"].Rows.Count; i++)
            {
                Model.Student objstud = new Model.Student();
                objstud.name = res.Tables["mentor"].Rows[i]["Name"].ToString();
                namesList.Add(objstud);
            }
            gvNames.DataSource = namesList;
            gvNames.DataBind();

        }
        private void getSuggestionid(int selectedrow)
        {
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolioConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT SuggestionID From Suggestion WHERE StudentID =@studentID ", conn);
            cmd.Parameters.AddWithValue("@studentID", studentid);
            SqlDataAdapter daSuggest = new SqlDataAdapter(cmd);
            DataSet res = new DataSet();
            conn.Open();
            daSuggest.Fill(res, "suggtable");
            conn.Close();
            suggestionID = Convert.ToInt32(res.Tables["suggtable"].Rows[0]["SuggestionID"]);
        }

        protected void gvNames_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int selectedrow = Convert.ToInt32(e.CommandArgument);
            newid = namesList[selectedrow].mentorID;
            getSuggestionid(selectedrow);
            Session["SelectedSuggestion"] = suggestionID.ToString();
            namesList.Clear();
            Response.Redirect("ViewSuggestion.aspx");


        }
    }
}
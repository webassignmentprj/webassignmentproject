﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="studentNav.ascx.cs" Inherits="webAssignmentR.Private.Student.studentNav" %>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
    crossorigin="anonymous">
<link href="studentNav.css" rel="stylesheet" />
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="studentNav.js"></script>


<div class="sideBar" >
    <div id="topSection" >
        <img src="../../Images/image3.png" / >
        <h1>Welcome,<br /></h1 >
        <asp:Label ID="lblName" runat="server"></asp:Label>
    </div >

    <ul >
        <li class="nav-item" >
            <a class="nav-link " href="editProf.aspx" >Edit Profile</a >
        </li >
        <li class="nav-item" >
            <a class="nav-link " href="ViewProj.aspx" > Project Portfolio</a >
        </li >
        <li class="nav-item" >
            <a class="nav-link " href="suggestionFrmMentor.aspx" > Suggestions from Mentor</a >
        </li >
        <li class="nav-item" >
            <asp:Button ID="btnLogOut" Text="Log Out" CssClass="btn btn-link nav-link"  style="font-size:30px" runat="server"  CausesValidation="False" OnClick="btnLogOut_Click" />
        </li >
    </ul >
</div>
<a class="btn"></a>

<script>
    $('.btn').on("click", function () {
    $('.btn').toggleClass('btnc');
    $('.sideBar').toggleClass('side');
})
</script>




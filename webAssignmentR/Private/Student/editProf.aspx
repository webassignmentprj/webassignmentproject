﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Private/Student/studentMaster.Master" AutoEventWireup="true" CodeBehind="editProf.aspx.cs" Inherits="webAssignmentR.Private.Student.editProf" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="allStudentPages.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <div class="container-fluid" align="center" style="width:85%;overflow: auto; position: relative;">
    <h1 style="text-align:left">Edit<br />
        Profile</h1>
        <table cellpadding="10px" style="height: 100%;width:100%; background-color: #F6F0FF; color: #302384;">
            <tr class="wrapper">
                <td class="col-md-2 col-sm-4 col-xs-5" style="height: 10%; font-size: 15px">Name:</td>
                <td class="col-md-10 col-sm-8 col-xs-7" style="height: 10%">
                    <asp:Label ID="lblName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr class="wrapper">
                
                    <td  class="col-md-2 col-sm-4 col-xs-5" style="height: 10%; font-size: 15px">Student ID:</td>
                    <td class="col-md-10 col-sm-8 col-xs-7" style="height: 10%">
                        <asp:Label ID="lblStudentID" runat="server"></asp:Label>
                    </td>
                
            </tr>
            <tr class="wrapper">
                
                    <td  class="col-md-2 col-sm-4 col-xs-5" style="height: 10%; font-size: 15px">Email:</td>
                    <td class="col-md-10 col-sm-8 col-xs-7" style="height: 10%">
                        <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail" ErrorMessage="Please put an email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                    </td>
                
            </tr>
            <tr class="wrapper">
                
                    <td class="col-md-2 col-sm-4 col-xs-5" style="height: 20%; font-size: 15px">Profile Picture:</td>
                    <td class="col-md-10 col-sm-8 col-xs-7"  >
                        <asp:Image ID="imgStudent" runat="server" Height="90"/>
                    </td>
                
            </tr>
            <tr class="wrapper">
                
                    <td class="col-md-2 col-sm-4 col-xs-5" style="height: 10%; font-size: 15px">&nbsp;</td>
                
                    <td class="col-md-10 col-sm-8 col-xs-7" style="height: 10%">
                        <asp:FileUpload ID="upPhoto" runat="server" />
                    </td>
                
            </tr>
            <tr class="wrapper">
                
                    <td class="col-md-2 col-sm-4 col-xs-5" style="height: 10%; font-size: 15px">&nbsp;</td>
                
                    <td class="col-md-10 col-sm-8 col-xs-7" style="height: 10%">
                        <asp:Button ID="btnUpload" runat="server" OnClick="btnUpload_Click" Text="Upload" />
                        <asp:Label ID="lblMsg" runat="server"></asp:Label>
                    </td>
                
            </tr>
            <tr class="wrapper">

                <td class="col-md-2 col-sm-4 col-xs-5" style="height: 35%; font-size: 15px">External Hyperlink:</td>
                <td class="col-md-10 col-sm-8 col-xs-7" style="height: 35%">
                    <asp:TextBox ID="txtHyperLink" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="rfvLink" runat="server" ControlToValidate="txtHyperLink" ErrorMessage="Please put a Proper URL" ValidationExpression="http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&amp;=]*)?"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr class="wrapper">

                <td class="col-md-2 col-sm-4 col-xs-5" style="height: 35%; font-size: 15px">Self-Description:</td>
                <td class="col-md-10 col-sm-8 col-xs-7" style="height: 35%">
                    <asp:TextBox ID="txtDesc" runat="server" Rows="10" TextMode="MultiLine" Height="80%" Width="70%"></asp:TextBox>
                </td>
            </tr>
            <tr class="wrapper">
                <td class="col-md-2 col-sm-4 col-xs-5" style="height: 35%; font-size: 15px">Achievements:</td>
                <td class="col-md-10 col-sm-8 col-xs-7" style="height: 35%">
                    <asp:TextBox ID="txtAchievement" runat="server" Rows="10" TextMode="MultiLine" Height="80%" Width="70%"></asp:TextBox>
                </td>
            </tr>
            <tr class="wrapper">
                <td class="col-md-2 col-sm-4 col-xs-5" style="height: 5%; font-size: 15px">Skill Sets:</td>
                <td class="col-md-10 col-sm-8 col-xs-7" style="height: 5%;">&nbsp;<asp:CheckBoxList ID="cblSkills" runat="server">
                </asp:CheckBoxList>
                </td>
            </tr>
            <tr class="wrapper">
                <td class="col-md-2 col-sm-4 col-xs-5"></td>
                <td class="col-md-10 col-sm-8 col-xs-7">
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

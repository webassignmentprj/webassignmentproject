﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Private/Student/studentMaster.Master" AutoEventWireup="true" CodeBehind="ProjCreate.aspx.cs" Inherits="webAssignmentR.Private.Student.ProjCreate" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="allStudentPages.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>  
    <script src="js/bootstrap.min.js"></script>    
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <div class="container-fluid" align="center" style=" width:85%; overflow:auto; position: relative;">
        
    <h1 style="text-align:left">Project Portfolio - Create</h1>
     <table cellpadding="10px" style=" width:100%; background-color: #F6F0FF; height:100%; position: relative;"  >
         <tr class="wrapper" style ="text-align:left; width:100%">
             <td class="col-lg-1 col-md-2 col-sm-4 col-xs-5" style="height: 10%; font-size: 15px">Project Name:</td>
             <td class="col-lg-11 col-md-10 col-sm-8 col-xs-7 "style="height: 10%;">
                 <asp:TextBox ID="txtName" runat="server" Width="40%"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" ErrorMessage="Please add a name">*</asp:RequiredFieldValidator>
             </td>
         </tr>
         <tr class="wrapper" style="width:100%;text-align:left">
             <td class="col-lg-1 col-md-2 col-sm-4 col-xs-5" style="height: 30%; font-size: 15px">Short Description:</td>
             <td class="col-lg-11 col-md-10 col-sm-8 col-xs-7" style="height: 30%;">
                 <asp:TextBox ID="txtDesc" runat="server" Height="90%" Width="60%" textmode="MultiLine" OnTextChanged="txtDesc_TextChanged"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="rfvDesc" runat="server" ControlToValidate="txtDesc" ErrorMessage="Add a description">*</asp:RequiredFieldValidator>
             </td>
         </tr>
         <tr class="wrapper"  style ="text-align:left;width:100%">

             <td class="col-lg-1 col-md-2 col-sm-4 col-xs-5" style="height: 10%; font-size: 15px">Add Group Members:</td>
             <td class="col-lg-11 col-md-10 col-sm-8 col-xs-7" style="height: 10%;">
                 <asp:TextBox ID="txtID" runat="server"></asp:TextBox>
                 <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click"/>
             &nbsp;<asp:RequiredFieldValidator ID="rfvMember" runat="server" ControlToValidate="txtID" ErrorMessage="Add at least 1 member">*</asp:RequiredFieldValidator>
                 <asp:RegularExpressionValidator ID="rfvNumeral" runat="server" ControlToValidate="txtID" ErrorMessage="Only Number Values" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                 <br />
                 Please add ALL ID(s)</td>
         </tr>
         <tr class="wrapper"style="width:100%; text-align:left">
             <td class="col-lg-1 col-md-2 col-sm-4 col-xs-5" style="height: 30%; font-size: 15px">Group Members:</td>
             <td class="col-lg-11 col-md-10 col-sm-8 col-xs-7" style="height: 30%;">
                 <asp:GridView ID="gvMemberList" runat="server" Width="70%" AutoGenerateColumns="False" ShowHeaderWhenEmpty="True" OnRowCommand="gvMemberList_RowCommand"  >
                     <Columns>
                         <asp:BoundField HeaderText="Name" DataField="Name" />
                         <asp:BoundField HeaderText="StudentID" DataField="studentID" />
                         <asp:ButtonField Text="remove" />
                     </Columns>
                 </asp:GridView>
             </td>
         </tr>
         <tr class="wrapper" style ="text-align:left; width:100%">
             <td class="col-lg-1 col-md-2 col-sm-4 col-xs-5" style="height: 10%; font-size: 15px">Project URL:</td>
             <td class="col-lg-11 col-md-10 col-sm-8 col-xs-7" style="height: 10%;">
                 <asp:TextBox ID="txtUrl" runat="server" Height="80%" Width="40%"></asp:TextBox>
                 <asp:RegularExpressionValidator ID="rgvURL" runat="server" ControlToValidate="txtUrl" ErrorMessage="Please put a proper URL" ValidationExpression="http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&amp;=]*)?"></asp:RegularExpressionValidator>
             </td>
         </tr>
         
         <tr class="wrapper" style ="text-align:left;width:100% ">
             <td class="col-lg-1 col-md-2 col-sm-4 col-xs-5" style="height: 20%; font-size: 15px">Poster Photo : (JPG format)</td>
             <td class="col-lg-11 col-md-10 col-sm-8 col-xs-7" style="height: 20%;">
                 <asp:Image CssClass="img-fluid" ID="imgPhoto" runat="server" Height="150px"  />
             </td>
         </tr>
         <tr class="wrapper" style ="text-align:left">
             <td class=" col-lg-1 col-md-2 col-sm-4 col-xs-5" style="height: 10%;"></td>
             <td class="col-lg-11 col-md-10 col-sm-8 col-xs-7"style="height: 10%;" >
                 <asp:FileUpload ID="upPhoto" runat="server" />
             </td>
         </tr>
         <tr class="wrapper" style="width:100%; text-align:left">
             <td class=" col-lg-1 col-md-2 col-sm-4 col-xs-5" style="height: 10%;  font-size: 15px"></td>
             <td class="col-lg-11 col-md-10 col-sm-8 col-xs-7" style="height: 10%;">
                 <asp:Button ID="btnUpload" runat="server" OnClick="btnUpload_Click" Text="Upload" />
                 <asp:Label ID="lblMsg" runat="server"></asp:Label>
             </td>
         </tr>
         <tr class="wrapper" style ="text-align:left;width:100%">
             <td class=" col-lg-1 col-md-2 col-sm-4 col-xs-5" style="height: 10%; font-size: 15px"></td>
             <td class="col-lg-11 col-md-10 col-sm-8 col-xs-7" style="height: 10%;">
                 <asp:Button ID="btnConfirm" runat="server" Text="Confirm" OnClick="btnConfirm_Click"/>
                 <asp:Label ID="lblCheckName" runat="server"></asp:Label>
                 <br />
                 <asp:Label ID="lblLeader" runat="server"></asp:Label>
             </td>
         </tr>
         <tr style ="text-align:left;width:100%">
             <td class="col-lg-1 col-md-2 col-sm-4 col-xs-5" style="height: 10%; font-size: 15px">&nbsp;</td>
             <td class="col-lg-11 col-md-10 col-sm-8 col-xs-7" style="height: 10%;">
                 &nbsp;</td>
         </tr>
     </table>
     
        </div>
</asp:Content>

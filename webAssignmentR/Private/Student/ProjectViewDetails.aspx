﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Private/Student/studentMaster.Master" AutoEventWireup="true" CodeBehind="ProjectViewDetails.aspx.cs" Inherits="webAssignmentR.Private.Student.ProjectViewDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="allStudentPages.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <div class="container-fluid" style="width:85%;overflow:auto; position: relative;">   
    <h1 style="text-align:left">Project Portfolio - View</h1>
        <table cellpadding="10px" style="width: 90%; background-color: #F6F0FF" >
            <tr class="wrapper">
                <td  class="col-md-12 col-sm-12 col-xs-12" align="center" style="height: 10%" colspan="2">
                    <asp:Label CssClass="labels" ID="lblTitle" runat="server" Width="600px" Font-Size="Large" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr class="wrapper">
                <td class="col-md-12 col-sm-12 col-xs-12" align="center" style="height: 10%" colspan="2">
                    <asp:Label  CssClass="labels" ID="lblGroupMembers" runat="server" Width="600px"></asp:Label>
                </td>
            </tr>
            <tr class="wrapper">
                <td class="col-md-12 col-sm-12 col-xs-12" align="center" style="height: 10%" colspan="2">
                    <asp:HyperLink CssClass="labels" ID="hlProjectURL" runat="server" Target="_blank">[hlProjectURL]</asp:HyperLink>
                </td>
            </tr>
            <tr class="wrapper">
                <td class="col-md-6 col-sm-6 col-xs-6">
                    <asp:Image CssClass="details" ID="imgPoster" runat="server" ImageUrl="." style="height:400px ; width:300px; position: relative"/>
                </td>
                <td class="col-md-6 col-sm-6 col-xs-6">
                    <asp:TextBox CssClass="details" ID="txtDesc" runat="server" ReadOnly="True" Height="300" Width="60%" Rows="100"  TextMode="MultiLine" ></asp:TextBox>
                </td>
            </tr>
            <tr class="wrapper">
                <td class="col-md-12 col-sm-12 col-xs-12" style="height: 10%" colspan="2">
                    &nbsp;
                    <asp:Button ID="btnBack" runat="server" OnClick="btnBack_Click" Text="Back" />
                    </td>
            </tr>
        </table>
           
    </div>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace webAssignmentR.Private.Student
{
    public partial class ProjectViewDetails : System.Web.UI.Page
    {
        Model.Project objproj = new Model.Project();
        string title;
        int id;
        List<int>studList = new List<int>();
        List<string> studNames = new List<string>();
        static int count = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Username"] != null)
            {
                if (!Page.IsPostBack)
                {
                    count = 0;
                    title = (string)Session["SelectedProj"];
                    lblTitle.Text = title;
                    getDesc();
                    getMembers();
                    getMemberName();
                    imgPoster.ImageUrl = "../../Images/Projects/" + objproj.poster;
                    hlProjectURL.Text = objproj.projectURL;
                    hlProjectURL.NavigateUrl = objproj.projectURL;
                    int i = 0;
                    foreach (string name in studNames)
                    {
                        if (i < studNames.Count() - 1)
                        {
                            lblGroupMembers.Text += name + ", ";
                        }
                        else
                        {
                            lblGroupMembers.Text += name;
                        }
                        i += 1;
                    }
                    txtDesc.Text = objproj.desc;
                }
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }
            
        }
        private void getDesc()
        {
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolioConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT * FROM Project WHERE Title =@Title ", conn);
            cmd.Parameters.AddWithValue("@Title", title);
            SqlDataAdapter daDesc = new SqlDataAdapter(cmd);
            DataSet res = new DataSet();
            conn.Open();
            daDesc.Fill(res, "Descrip");
            conn.Close();
            if (res.Tables["Descrip"].Rows[0]["Description"].ToString() != null)
            {
                objproj.desc = res.Tables["Descrip"].Rows[0]["Description"].ToString();
            }
            else
            {
                objproj.desc = "No Description";
            }
            id = Convert.ToInt32(res.Tables["Descrip"].Rows[0]["ProjectID"]);
            objproj.poster = res.Tables["Descrip"].Rows[0]["ProjectPoster"].ToString();
            objproj.projectURL = res.Tables["Descrip"].Rows[0]["ProjectURL"].ToString();

        }
        private void getMembers()
        {
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolioConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT * FROM ProjectMember WHERE ProjectID =@id", conn);
            cmd.Parameters.AddWithValue("@id", id);
            SqlDataAdapter daMember = new SqlDataAdapter(cmd);
            DataSet res = new DataSet();
            conn.Open();
            daMember.Fill(res, "Member");
            conn.Close();
            foreach(DataRow dr in res.Tables["Member"].Rows)
            {
                int studentID = Convert.ToInt32(res.Tables["Member"].Rows[count]["studentID"]);
                studList.Add(studentID);
                count += 1;
            }
            
        }
        private void getMemberName()
        {
            foreach (int studentID in studList)
            {
                string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolioConnectionString"].ToString();
                SqlConnection conn = new SqlConnection(strConn);
                SqlCommand cmd = new SqlCommand("SELECT * FROM Student WHERE StudentID =@studentID", conn);
                cmd.Parameters.AddWithValue("@studentID", studentID);
                SqlDataAdapter daName = new SqlDataAdapter(cmd);
                DataSet res = new DataSet();
                conn.Open();
                daName.Fill(res, "Names");
                conn.Close();
                string names = res.Tables["Names"].Rows[0]["Name"].ToString();
                studNames.Add(names);
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("ViewProj.aspx");
        }
    }
}
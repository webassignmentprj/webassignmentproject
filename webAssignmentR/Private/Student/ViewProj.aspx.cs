﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace webAssignmentR.Private.Student
{
    public partial class ViewProj : System.Web.UI.Page
    {
        Model.Student objStudent = new Model.Student();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Username"] != null)
            {
                string id = (string)Session["UserID"];
                objStudent.studentID = Convert.ToInt32(id);
                displayProj();
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }
        }
        private void displayProj()
        {
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolioConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT Title,ProjectPoster FROM Project " +
                "INNER JOIN ProjectMember " +
                "ON Project.ProjectID = ProjectMember.ProjectID " +
                "AND ProjectMember.StudentID = @memberID",conn);
            cmd.Parameters.AddWithValue("@memberID", objStudent.studentID);
            SqlDataAdapter daProj = new SqlDataAdapter(cmd);
            DataSet res = new DataSet();
            conn.Open();
            daProj.Fill(res, "ProjNames");
            conn.Close();
            gvProj.DataSource = res.Tables["ProjNames"];
            gvProj.DataBind();
            int count = gvProj.Rows.Count;
            lblTotal.Text = "Number of Projects:" + count;
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            Response.Redirect("ProjCreate.aspx");
        }

        protected void gvProj_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int selectedrow = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = gvProj.Rows[selectedrow];
            string title = row.Cells[0].Text;
            Session["SelectedProj"] = title;
            if(e.CommandName == "Update")
            {
                Response.Redirect("UpdateProj.aspx");
            }
            if(e.CommandName == "View")
            {
                Response.Redirect("ProjectViewDetails.aspx");
            }
        }

        protected void gvProj_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvProj.PageIndex = e.NewPageIndex;
            displayProj();
        }
    }
}
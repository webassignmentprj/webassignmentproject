﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Private/Student/studentMaster.Master" AutoEventWireup="true" CodeBehind="ViewProj.aspx.cs" Inherits="webAssignmentR.Private.Student.ViewProj" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="allStudentPages.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <div class="container-fluid" align="center" style=" width:85%; overflow: auto;">
    <h1 style ="text-align:left">Project Portfolio</h1>
    <table cellpadding="10px"  style=" width:100%; height: 100%; background-color: #F0F6FF; position: relative;">
        <tr class="wrapper">
            <td class="col-md-10 col-sm-8 col-xs-7" style="height: 10%;"></td>
            <td class="col-md-2 col-sm-4 col-xs-5" style="height: 10%">
                <asp:Button ID="btnCreate" runat="server" Text="Create New" OnClick="btnCreate_Click" />
            </td>
        </tr>
        <tr class="wrapper">
            <td class="col-md-10 col-sm-8 col-xs-7" style=" height: 70%;">
                <asp:GridView ID="gvProj" runat="server" Width="100%" BackColor="White" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" CellPadding="4" AutoGenerateColumns="False" OnRowCommand="gvProj_RowCommand" AllowPaging="True" OnPageIndexChanging="gvProj_PageIndexChanging" PageSize="5" style="position: relative" >
                    <Columns>
                        <asp:BoundField DataField="Title" HeaderText="Title"  >
                        <HeaderStyle Width="30%" />
                        <ItemStyle Width="30%" />
                        </asp:BoundField>
                        <asp:ImageField DataImageUrlField="ProjectPoster" DataImageUrlFormatString="../../Images/Projects/{0}" HeaderText="Poster" ItemStyle-HorizontalAlign="Center" >
                            <ControlStyle Width="50px" />
                            <HeaderStyle Width="30%" />

<ItemStyle HorizontalAlign="Center" Width="30%"></ItemStyle>
                        </asp:ImageField>
                        <asp:ButtonField Text="Update" commandname="Update">
                        <HeaderStyle Width="20%" />
                        <ItemStyle Width="20%" />
                        </asp:ButtonField>
                        <asp:ButtonField Text="View" commandname="View" >
                        <HeaderStyle Width="20%" />
                        <ItemStyle Width="20%" />
                        </asp:ButtonField>
                    </Columns>
                    <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                    <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
                    <PagerStyle BackColor="#004bc0" ForeColor="White"  HorizontalAlign="center" />
                    <RowStyle BackColor="White" ForeColor="#003399" />
                    <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                    <SortedAscendingCellStyle BackColor="#EDF6F6" />
                    <SortedAscendingHeaderStyle BackColor="#0D4AC4" />
                    <SortedDescendingCellStyle BackColor="#D6DFDF" />
                    <SortedDescendingHeaderStyle BackColor="#002876" />
                </asp:GridView>
            </td>
            <td class="col-md-2 col-sm-4 col-xs-5" style="height: 70%"></td>
        </tr>
        <tr class="wrapper">
            <td class="col-md-10 col-sm-8 col-xs-7" style=" height:20%">
                <asp:Label ID="lblTotal" runat="server"></asp:Label>
            </td>
            <td class="col-md-2 col-sm-4 col-xs-5" style="height: 20%"></td>
        </tr>
    </table>
</div>
</asp:Content>
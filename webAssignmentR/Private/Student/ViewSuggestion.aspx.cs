﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace webAssignmentR.Private.Student
{
    public partial class ViewSuggestion : System.Web.UI.Page
    {
        static int suggestionID;
        static int mentorID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Username"] != null)
            {
                string id = (string)Session["SelectedSuggestion"];
                suggestionID = Convert.ToInt32(id);
                updateReadStatus();
                getSuggestion();
                getMentorName();

            }
            else
            {
                Response.Redirect("~/login.aspx");
            }
        }
        private void getSuggestion()
        {
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolioConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT Description,DateCreated,MentorID FROM Suggestion WHERE SuggestionID =@suggid", conn);
            cmd.Parameters.AddWithValue("@suggid",suggestionID);
            SqlDataAdapter daSuggestion = new SqlDataAdapter(cmd);
            DataSet res = new DataSet();
            conn.Open();
            daSuggestion.Fill(res, "MentorSuggestion");
            conn.Close();
            txtSuggestion.Text = res.Tables["MentorSuggestion"].Rows[0]["Description"].ToString();
            lblDateTime.Text = res.Tables["MentorSuggestion"].Rows[0]["DateCreated"].ToString();
            mentorID = Convert.ToInt32(res.Tables["MentorSuggestion"].Rows[0]["MentorID"]);
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("suggestionFrmMentor.aspx");
        }
        private void updateReadStatus()
        {
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolioConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("UPDATE Suggestion SET Status = 'Y' WHERE SuggestionID =@suggid", conn);
            cmd.Parameters.AddWithValue("@suggid", suggestionID);
            SqlDataAdapter daStatus = new SqlDataAdapter(cmd);
            DataSet res = new DataSet();
            conn.Open();
            int count = cmd.ExecuteNonQuery();
            conn.Close();
        }
        private void getMentorName()
        {
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolioConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT Name FROM Mentor WHERE MentorID =@mentorid", conn);
            cmd.Parameters.AddWithValue("@mentorid", mentorID);
            SqlDataAdapter daMentor = new SqlDataAdapter(cmd);
            DataSet res = new DataSet();
            conn.Open();
            daMentor.Fill(res, "MentorName");
            conn.Close();
            lblMentor.Text = res.Tables["MentorName"].Rows[0]["Name"].ToString();
        }
    }
}
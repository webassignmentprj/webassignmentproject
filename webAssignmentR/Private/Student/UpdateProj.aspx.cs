﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace webAssignmentR.Private.Student
{
    public partial class UpdateProj : System.Web.UI.Page
    {
        static List<Model.Student> memberlist = new List<Model.Student>();
        static int studentid = 0;
        Model.Project objProj = new Model.Project();
        static string currStudent;
        static int selectedrow = 0;
        static int projid;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Username"] != null)
            {
                if (!Page.IsPostBack)
                {

                    memberlist.Clear();
                    studentid = 0;
                    objProj.title = (string)Session["SelectedProj"];
                    if (Session["Username"] != null)
                    {
                        string studeID = (string)Session["UserID"];
                        currStudent = (string)Session["Username"];
                        studentid = Convert.ToInt32(studeID);
                    }
                    getProjID();
                    getMembersDesc();
                    txtDesc.Text = objProj.desc;
                    txtName.Text = objProj.title;
                    txtURL.Text = objProj.projectURL;
                    imgProject.ImageUrl = "../../Images/Projects/" + objProj.poster;
                }

            }
            else
            {
                Response.Redirect("~/login.aspx");
            }
        }
            private void getProjID()
        {
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolioConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT ProjectID FROM Project WHERE Title =@title", conn);
            cmd.Parameters.AddWithValue("@title", objProj.title);
            SqlDataAdapter daID = new SqlDataAdapter(cmd);
            DataSet res = new DataSet();
            conn.Open();
            daID.Fill(res, "ProjID");
            conn.Close();
            projid = Convert.ToInt32(res.Tables["ProjID"].Rows[0]["ProjectID"]);
        }
        private void getMembersDesc()
        {
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolioConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT Student.StudentID, Name FROM Student INNER JOIN ProjectMember ON Student.StudentID = ProjectMember.StudentID WHERE ProjectMember.ProjectID =@id  ", conn);
            SqlCommand command = new SqlCommand("SELECT Description,ProjectPoster,ProjectURL FROM Project WHERE ProjectID =@id",conn);
            cmd.Parameters.AddWithValue("@id", projid);
            command.Parameters.AddWithValue("@id", projid);
            SqlDataAdapter daMembers = new SqlDataAdapter(cmd);
            SqlDataAdapter daDesc = new SqlDataAdapter(command);
            DataSet result = new DataSet();
            DataSet res = new DataSet();
            conn.Open();
            daMembers.Fill(res, "ProjID");
            daDesc.Fill(result, "Details");
            conn.Close();
            for(int i = 0; i < res.Tables["ProjID"].Rows.Count;i++)
            {
                Model.Student objstudent = new Model.Student();
                objstudent.name = res.Tables["ProjID"].Rows[i]["Name"].ToString();
                objstudent.studentID = Convert.ToInt32(res.Tables["ProjID"].Rows[i]["StudentID"]);
                memberlist.Add(objstudent);
            }
            gvMemberList.DataSource = memberlist;
            gvMemberList.DataBind();
            objProj.desc = result.Tables["Details"].Rows[0]["Description"].ToString();
            objProj.projectURL = result.Tables["Details"].Rows[0]["ProjectURL"].ToString();
            objProj.poster = result.Tables["Details"].Rows[0]["ProjectPoster"].ToString();

        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            int count = 0;
            for (int i = 0; i < memberlist.Count; i++)
            {
                if (studentid == memberlist[i].studentID)
                {
                    count += 1;
                }
            }
            if (count > 0)
            {
                updateTitleandDesc();
                clearAllGroupMember();
                updateGroupMembers();
                Response.Redirect("ViewProj.aspx");
            }
            else
            {
                lblLeader.Text = "Please add Your Student ID!";
            }

        }
        public void updateTitleandDesc()
        {
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolioConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("UPDATE Project SET Title=@title, Description=@desc, ProjectPoster=@poster, ProjectURL=@url WHERE ProjectID =@projectid", conn);
            cmd.Parameters.AddWithValue("@title", txtName.Text);
            cmd.Parameters.AddWithValue("@desc", txtDesc.Text);
            cmd.Parameters.AddWithValue("@projectid", projid);
            cmd.Parameters.AddWithValue("@poster", imgProject.ImageUrl.ToString());
            cmd.Parameters.AddWithValue("@url", txtURL.Text);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            
        }
        public void clearAllGroupMember()
        {
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolioConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("DELETE FROM ProjectMember WHERE ProjectID =@projectid", conn);
            cmd.Parameters.AddWithValue("@projectid", projid);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }
        public void updateGroupMembers()
        {
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolioConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            for(int i = 0; i < memberlist.Count(); i++)
            {
                SqlCommand command = new SqlCommand("INSERT INTO ProjectMember(ProjectID,StudentID,Role)VALUES(@projid,@stuid,@role)", conn);
                command.Parameters.AddWithValue("@projid", projid );
                command.Parameters.AddWithValue("@stuid", memberlist[i].studentID);
                if (studentid == memberlist[i].studentID)
                {
                    command.Parameters.AddWithValue("@role", "Leader");
                }
                else
                {
                    command.Parameters.AddWithValue("@role", "Member");
                }
                conn.Open();
                command.ExecuteNonQuery();
                conn.Close();
            }
        }
        public void addMembers()
        {
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolioConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT StudentID, Name FROM Student WHERE StudentID = @studentID", conn);
            cmd.Parameters.AddWithValue("@studentID", Convert.ToInt32(txtID.Text));
            SqlDataAdapter daMember = new SqlDataAdapter(cmd);
            DataSet res = new DataSet();
            conn.Open();
            daMember.Fill(res, "Projmember");
            conn.Close();
            int num = 0;
            if (memberlist.Count() > 0)
            {
                for (int i = 0; i < memberlist.Count(); i++)
                {
                    int id = Convert.ToInt32(txtID.Text);
                    if (id == memberlist[i].studentID)
                    {
                        num += 1;
                    }
                    if (i == memberlist.Count() - 1 & num == 0)
                    {
                        Model.Student objstudent = new Model.Student();
                        objstudent.name = res.Tables["Projmember"].Rows[0]["Name"].ToString();
                        objstudent.studentID = Convert.ToInt32(res.Tables["Projmember"].Rows[0]["StudentID"]);
                        memberlist.Add(objstudent);
                        gvMemberList.DataSource = memberlist;
                        gvMemberList.DataBind();
                    };
                }
            }
            else
            {
                Model.Student leader = new Model.Student();
                leader.name = currStudent;
                leader.studentID = studentid;
                memberlist.Add(leader);
                int id = Convert.ToInt32(txtID.Text);
                if (studentid != id)
                {
                    Model.Student objstudent = new Model.Student();
                    objstudent.name = res.Tables["Projmember"].Rows[0]["Name"].ToString();
                    objstudent.studentID = Convert.ToInt32(res.Tables["Projmember"].Rows[0]["StudentID"]);
                    memberlist.Add(objstudent);
                    gvMemberList.DataSource = memberlist;
                    gvMemberList.DataBind();
                }
                else
                {
                    gvMemberList.DataSource = memberlist;
                    gvMemberList.DataBind();
                }
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            addMembers();
        }

        private void removeMember()
        {
            memberlist.RemoveAt(selectedrow);
        }
        protected void gvMemberList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            selectedrow = Convert.ToInt32(e.CommandArgument);
            removeMember();
            gvMemberList.DataSource = memberlist;
            gvMemberList.DataBind();
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {

            string uploadedFile = "";
            if (upPhoto.HasFile == true)
            {
                string savepath;
                string fileExt = Path.GetExtension(upPhoto.FileName);
                uploadedFile = fileExt;
                savepath = MapPath("../../Images/Projects/" + uploadedFile);
                try
                {
                    upPhoto.SaveAs(savepath);
                    lblMessage.Text = "File uploaded successfully";
                    imgProject.ImageUrl = "../../Images/Projects/" + uploadedFile;

                }
                catch (IOException)
                {
                    lblMessage.Text = "File uploading fail!";
                }
                catch (Exception ex)
                {
                    lblMessage.Text = ex.Message;
                }

            }
        }
    }
}
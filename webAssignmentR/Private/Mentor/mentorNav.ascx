﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="mentorNav.ascx.cs" Inherits="webAssignmentR.Private.Mentor.mentorNav" %>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
<link href="mentorNav.css" rel="stylesheet" />
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="mentorNav.js"></script>


<div class="sideBar" >
    <div id="topSection" >
        <img src="../../Images/image3.png" / >
        <h1>Welcome, <asp:Label ID="lblName" runat="server" CssClass="mentorStyle"></asp:Label></h1 >
        
    </div >

    <ul >
        <li class="nav-item" >
            <a class="nav-link " href="viewStudProf.aspx" >View Student Profile</a >
        </li >
        <li class="nav-item" >
            <a class="nav-link " href="approvePort.aspx" > Approve Portfolio</a >
        </li >
        <li class="nav-item" >
            <a class="nav-link " href="findStud.aspx" > Find Students</a >
        </li >
        <li class="nav-item" >
            <a class="nav-link " href="inbox.aspx" > Inbox</a >
        </li >
        <li class="nav-item" >
            <a class="nav-link " href="accSetting.aspx" > Account Setting </a >
        </li >
        <li class="nav-item" >
            <asp:Button ID="btnLogOut" Text="Log Out" CssClass="btn btn-link nav-link"  style="font-size:30px" runat="server"  CausesValidation="False" OnClick="btnLogOut_Click" />
        </li >
      
    </ul >
</div>
<a class="btn"></a>

<script>
    $('.btn').on("click", function () {
    $('.btn').toggleClass('btnc');
    $('.sideBar').toggleClass('side');
})
</script>


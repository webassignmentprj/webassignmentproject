﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Private/Mentor/MentorMaster.Master" AutoEventWireup="true" CodeBehind="confirmApproval.aspx.cs" Inherits="webAssignmentR.Private.Mentor.confirmApproval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <p>
        Are you sure you want to approve the portfolio of&nbsp;
        <asp:Label ID="lblStudentName" runat="server"></asp:Label>
        ?
    </p>
    <p>
        <asp:Button ID="btnYes" runat="server" Text="Yes" OnClick="btnYes_Click" />
        <asp:Button ID="btnNo" runat="server" Text="No" />
    </p>

    <asp:Label ID="lblMsg" runat="server"></asp:Label>
    <br />
    <asp:HyperLink ID="hiddenLink" runat="server" NavigateUrl="~/Private/Mentor/approvePort.aspx" Visible="False">[HyperLink1]</asp:HyperLink>
</asp:Content>

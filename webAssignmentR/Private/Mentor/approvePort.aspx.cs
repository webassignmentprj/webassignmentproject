﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;


namespace webAssignmentR.Private.Mentor
{
    public partial class approvePort : System.Web.UI.Page
    {
        //list all the portfolioo that has the status of MN and decide weather or not to approve them
        //List all the status
        protected void Page_Load(object sender, EventArgs e)
        {
            displayPortStatus();


        }

        private void displayPortStatus()
        {
            DataSet unApproved = DatabaseProxy.Shared.selectCmd("Status", "select * from Student Where Status = 'N'");
            gvPortStats.DataSource = unApproved.Tables["Status"];
            gvPortStats.DataBind();

        }
    }
}
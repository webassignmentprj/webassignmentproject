﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Private/Mentor/MentorMaster.Master" AutoEventWireup="true" CodeBehind="accSetting.aspx.cs" Inherits="webAssignmentR.Private.Mentor.accSetting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="allMentorPages.css" rel="stylesheet" />
    <h1>Update Settings</h1>
    <br />
    <div class="row">
        <div class="d-flex flex-column w-100">
            <div class="container-fluid boxOutline">

                <h2>Change Settings</h2>
                <div class="form-group">
                    <label>Password:</label>
                    <asp:TextBox runat="server" ID="tbPwd" CssClass="form-control" type="password" placeholder="Enter password"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="regexLength" runat="server" ControlToValidate="tbPwd" Text="*" ErrorMessage="Please enter a password is more than 8 characters" ValidationExpression="^.{8,}"></asp:RegularExpressionValidator>
                </div>

                <div class="form-group">
                    <label>Confirm Password:</label>
                    <asp:TextBox runat="server" ID="tbConfirmPwd" CssClass="form-control" type="password" placeholder="Enter password"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="tbPwd" Text="*" ErrorMessage="Please enter a password is more than 8 characters" ValidationExpression="^.{8,}"></asp:RegularExpressionValidator>
                </div>

                <div class="d-flex flex-column">
                    <asp:Button type="submit" runat="server" ID="btnChange" Text="Confirm?" CssClass="btn btn-primary w-50" OnClick="btnChange_Click" />
                    <p>
                        <asp:Label ID="ShowConfirmation" runat="server"></asp:Label><br />
                        <asp:Label ID="ShowError" runat="server" ForeColor="Red"></asp:Label>
                        <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Password Mismatch" ControlToValidate="tbPwd" ControlToCompare="tbConfirmPwd"></asp:CompareValidator>
                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" />


                </div>

            </div>





        </div>
    </div>
</asp:Content>

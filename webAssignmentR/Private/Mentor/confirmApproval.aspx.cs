﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace webAssignmentR.Private.Mentor
{
    public partial class confirmApproval : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblStudentName.Text = Request.QueryString["Name"];

        }

        protected void btnYes_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["StudentID"] != null)
            {
                Model.Mentor objMentor = new Model.Mentor();
                int studentid = Convert.ToInt32((Request.QueryString["StudentID"]));
                objMentor.studentID = studentid;
                int errorCode = objMentor.update();
                

                if(errorCode == 0)
                {
                    lblMsg.Text = "Status has changed to approved!";
                    hiddenLink.Visible = true;
                    hiddenLink.Text = "Return back to where you were";
                }

            }

        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Private/Mentor/MentorMaster.Master" AutoEventWireup="true" CodeBehind="findStud.aspx.cs" Inherits="webAssignmentR.Private.Mentor.findStud" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="allMentorPages.css" rel="stylesheet" />
    <h1>Find Students</h1>
    <p>Input Student Name:</p>
    <div class="d-flex flex-row" style="height: 40px;">
        <asp:CheckBoxList ID="chkBxLiSkillSet" runat="server" RepeatDirection="Horizontal" CssClass="chkBxStyle" RepeatLayout="Flow"></asp:CheckBoxList>
        <asp:Button runat="server" ID="btnFindStudent" Text="Find!" CssClass="searchBtn btn btn-primary ml-2" OnClick="btnFindStudent_Click"></asp:Button>
    </div>
    <br />

    <asp:Label ID="showError" runat="server"></asp:Label>


    <div class="container d-flex flex-row justify-content-between">
        <asp:GridView ID="gvDisplayResults" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <EditRowStyle BackColor="#999999" />
            <EmptyDataTemplate>
                No Students to show
            </EmptyDataTemplate>
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
        </asp:GridView>
        <asp:GridView ID="gvTemp" runat="server"></asp:GridView>
        <asp:GridView ID="gvShowStudent" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <EditRowStyle BackColor="#999999" />
            <EmptyDataTemplate>
                Nothing to show bro
            </EmptyDataTemplate>
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
        </asp:GridView>


    </div>


</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace webAssignmentR.Private.Mentor
{
    public partial class findStud : System.Web.UI.Page
    {
        List<string> selectedChkbox = new List<string>();
        List<int> skillSetID = new List<int>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                getStudentSkillset();
            }
        }

        public void getStudentSkillset()
        {
            DataSet daSkillSet = DatabaseProxy.Shared.selectCmd("SkillSets", "Select * from SkillSet");
            chkBxLiSkillSet.DataSource = daSkillSet.Tables["SkillSets"];
            chkBxLiSkillSet.DataValueField = "SkillSetName";
            chkBxLiSkillSet.DataBind();

        }

        protected void btnFindStudent_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                try
                {
                    //Retrieve selected checkboxes value
                    for (var i = 0; i < chkBxLiSkillSet.Items.Count; i++)
                    {
                        if (chkBxLiSkillSet.Items[i].Selected)
                        {
                            selectedChkbox.Add(chkBxLiSkillSet.Items[i].Value);
                        }

                    }

                    for (var i = 0; i < selectedChkbox.Count; i++)//This is to get the skillset ID
                    {
                        DataSet dsSkill = DatabaseProxy.Shared.selectCmd("skillID", $"select * from SkillSet WHERE SKillSetName = '{selectedChkbox[i]}'");
                        skillSetID.Add(Convert.ToInt32(dsSkill.Tables["skillID"].Rows[0]["SkillSetID"]));
                    }


                    //String builder
                    StringBuilder builder = new StringBuilder();
                    foreach (var ID in skillSetID)
                    {
                        builder.Append("SS.SkillSetID = ").Append(ID).Append(" OR ");
                    }
                    string builderQuery = builder.ToString().Remove(builder.Length - 3).TrimEnd();

                    for (var i = 0; i < skillSetID.Count; i++)//This is to display the data
                    {
                        DataSet dsFilteredStd = DatabaseProxy.Shared.selectCmd("Filtered",
                            "select Distinct(S.Name) from " +
                            "SkillSet SS inner join StudentSkillSet SKS " +
                            "ON SS.SkillSetID = SKS.SkillSetID inner join Student S " +
                            $"ON SKS.StudentID = S.StudentID WHERE {builderQuery}");
                        gvShowStudent.DataSource = dsFilteredStd.Tables["Filtered"];


                    }

                    gvShowStudent.DataBind();

                }
                catch
                {
                    showError.Text = "Please select a button!!";
                }
                
            }
        }
    }
}

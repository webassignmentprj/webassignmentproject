﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace webAssignmentR.Private.Mentor
{
    public partial class viewStudProf : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            displayStudentProfile();
        }

        private void displayStudentProfile()
        {
            DataSet Ds =  DatabaseProxy.Shared.selectCmd("Student", $"select * from Student");
            gvPortfolio.DataSource = Ds.Tables["Student"];
            gvPortfolio.DataBind();
        }
    }
}
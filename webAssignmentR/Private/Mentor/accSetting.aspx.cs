﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace webAssignmentR.Private.Mentor
{
    public partial class accSetting : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            displayCurDetails();

        }

        private void displayCurDetails()
        {
            //Get Mentor ID
            string strMentorID = (string)Session["UserID"];
            int MentorID = Int32.Parse(strMentorID);

            //Get email addr
            DataSet dsEmail = DatabaseProxy.Shared.selectCmd("email", $"select EmailAddr from Mentor WHERE MentorID = {MentorID}");


        }

        protected void btnChange_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                string strMentorID = (string)Session["UserID"];
                int MentorID = Int32.Parse(strMentorID);
                string pwd = tbPwd.Text;
                string confirmPwd = tbConfirmPwd.Text;
                if (confirmPwd.Any(c => char.IsDigit(c)))
                {
                    DatabaseProxy.Shared.nonQueryCmd("MentorPwd", "UPDATE Mentor " +
                        $"SET Password = '{pwd}' " +
                        $"WHERE MentorID = {MentorID}");
                    tbPwd.Text = "";
                    ShowConfirmation.Text = "Password has been changed";
                }
                else
                {
                    ShowConfirmation.Text = "Your Password does not contain a digit!";
                    pwd = "";
                    confirmPwd = "";
                }






            }
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Private/Mentor/MentorMaster.Master" AutoEventWireup="true" CodeBehind="inbox.aspx.cs" Inherits="webAssignmentR.Private.Mentor.inbox" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid">
        <div class="row">
            <div class="flex-column">


                <link href="allMentorPages.css" rel="stylesheet" />
                <h1>View Messages</h1>
                <asp:GridView ID="gvViewMsg" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
                <br />
                <h1>Reply Messages</h1>
                <p>Choose the Message ID you want to reply to</p>
                <asp:DropDownList ID="ddlMsgID" runat="server"></asp:DropDownList>
                <asp:Label runat="server" ID="lblnoSelID"></asp:Label>


                <div class="form-group">
                    <label>Type your message here</label>
                    <asp:TextBox type="text" CssClass="form-control" TextMode="MultiLine" Height="100" ID="tbReply" runat="server" Rows="3"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="tbReply" ErrorMessage="Please enter a message to reply"></asp:RequiredFieldValidator>

                </div>
                <asp:Button runat="server" ID="btnReply" Text="Reply" CssClass="btn btn-primary" OnClick="btnReply_Click" />
            </div>
        </div>
    </div>


</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace webAssignmentR.Private.Mentor
{
    public partial class inbox : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                displayMsg();
                msgID();
            }
        }

        private void displayMsg()
        {
            string strMentorID = (string)Session["UserID"];
            int MentorID = Int32.Parse(strMentorID);
            DataSet dsMsg = DatabaseProxy.Shared.selectCmd("messages", $"select MessageID, Title, Text from Message where ToID = {MentorID}");
            gvViewMsg.DataSource = dsMsg.Tables["messages"];
            gvViewMsg.DataBind();
        }

        private void msgID()
        {
            string strMentorID = (string)Session["UserID"];
            int MentorID = Int32.Parse(strMentorID);
            DataSet dsMsg = DatabaseProxy.Shared.selectCmd("msgID", $"select MessageID from Message where ToID = {MentorID}");
            ddlMsgID.DataSource = dsMsg.Tables["msgID"];
            ddlMsgID.DataValueField = "MessageID";
            ddlMsgID.DataBind();
            ddlMsgID.Items.Insert(0, "--select--");
        }

        protected void btnReply_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                string strMentorID = (string)Session["UserID"];
                int MentorID = Int32.Parse(strMentorID);
                string reply = tbReply.Text;
                if (ddlMsgID.SelectedIndex == 0)
                {
                    lblnoSelID.Text = "Please select an ID";
                }
                else
                {
                    int messageID = Convert.ToInt32(ddlMsgID.SelectedValue);
                    DatabaseProxy.Shared.nonQueryCmd("updateReplyTbl",
                        $"INSERT INTO Reply (MessageID, MentorID, Text) VALUES ({messageID}, {MentorID}, '{reply}')");
                    tbReply.Text = "";
                    lblnoSelID.Text = "";
                    ddlMsgID.SelectedIndex = 0;
                    Response.Write("<script>alert('Message successfully sent!')</script>");

                }

            }




        }


    }
}